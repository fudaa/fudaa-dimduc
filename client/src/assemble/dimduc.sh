#!/bin/bash
cd `dirname $0`
java -Xmx1024m -Xms512m -cp "$PWD/Fudaa-Dimduc-${artifact.version}.jar" org.fudaa.fudaa.dimduc.Dimduc $1 $2 $3 $4 $5 $6 $7 $8 $9
