/*
 * @file         DimducAstuces.java
 * @creation     1999-03-01
 * @modification $Date: 2007-01-19 13:14:38 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.dimduc;
import com.memoire.bu.BuPreferences;

import org.fudaa.fudaa.commun.FudaaAstuces;
import org.fudaa.fudaa.commun.FudaaAstucesAbstract;
/**
 * @version      $Revision: 1.9 $ $Date: 2007-01-19 13:14:38 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class DimducAstuces extends FudaaAstucesAbstract {
  public static DimducAstuces DIMDUC= new DimducAstuces();
  protected FudaaAstucesAbstract getParent() {
    return FudaaAstuces.FUDAA;
  }
  protected BuPreferences getPrefs() {
    return DimducPreferences.DIMDUC;
  }
  /*
  public static void main( String argv[])
  {
    System.out.println("nb ligne de Fudaa "+FudaaAstuces.FUDAA.getNombreLigne());
    System.out.println("nb ligne de Dimduc "+DimducAstuces.DIMDUC.getNombreLigne());
    System.out.println(DimducAstuces.DIMDUC.getAstuce());
  }*/
}
