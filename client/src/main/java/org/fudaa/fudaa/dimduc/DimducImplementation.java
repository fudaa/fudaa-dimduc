/*
 * @file DimducImplementation.java
 * @creation 2001-01-24
 * @modification $Date: 2007-05-04 13:59:05 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.fudaa.dimduc;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.print.PrinterJob;
import java.beans.PropertyVetoException;
import java.io.File;
import java.util.List;
import java.util.Map;

import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;

import com.memoire.bu.*;
import com.memoire.fu.Fu;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.table.CtuluTableColumnHeader;

import org.fudaa.dodico.corba.dimduc.*;
import org.fudaa.dodico.corba.objet.IConnexion;

import org.fudaa.dodico.dimduc.DCalculDimduc;

import org.fudaa.ebli.impression.EbliFillePrevisualisation;
import org.fudaa.ebli.impression.EbliMiseEnPageDialog;
import org.fudaa.ebli.impression.EbliMiseEnPagePreferencesPanel;
import org.fudaa.ebli.impression.EbliPageable;
import org.fudaa.ebli.ressource.EbliResource;

import org.fudaa.fudaa.commun.FudaaAstucesAbstract;
import org.fudaa.fudaa.commun.FudaaBrowserControl;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.aide.FudaaAidePreferencesPanel;
import org.fudaa.fudaa.commun.aide.FudaaAstucesDialog;
import org.fudaa.fudaa.commun.dodico.FudaaDodicoTacheConnexion;
import org.fudaa.fudaa.commun.dodico.FudaaImplementation;
import org.fudaa.fudaa.commun.impl.FudaaFilleTableau;
import org.fudaa.fudaa.commun.projet.FudaaFiltreFichier;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.commun.projet.FudaaProjetInformationsFrame;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.utilitaire.ServeurCopieEcran;

/**
 * L'implementation du client Dimduc.
 *
 * @version $Id: DimducImplementation.java,v 1.25 2007-05-04 13:59:05 deniger Exp $
 * @author Christian Barou
 */
public class DimducImplementation extends FudaaImplementation {

  // public final static String LOCAL_UPDATE = ".";

  public final static boolean IS_APPLICATION = true;

  public ICalculDimduc serveurDimduc_;

  public final static BuInformationsSoftware DIMDUC_SOFT = new BuInformationsSoftware();

  protected BuInformationsDocument idDimduc_ = new BuInformationsDocument();

  public final static String UNITE_EFFORT = "t.m.";

  private IParametresDimduc dimducParams_;

  private IResultatsDimduc dimducResults_;

  protected final static int RECENT_COUNT = 10;

  // concerne les donn�es :
  protected FudaaProjet projet_ = null; // projet
  // en
  // cours
  protected STableauMoments[] resultatsML_ = null, resultatsMC_ = null;

  private boolean premiereFoisParametres_;

  protected SSollicitationAccostage[] resultatsDL_ = null, resultatsDC_ = null, resultatsTS_ = null;

  protected DimducFilleParametres fp_ = null;

  protected DimducValidation dv_ = null;

  protected FudaaFilleTableau fTableau_ = null;

  protected DimducPanneauDuc pdl_ = null, pdc_ = null;

  protected EbliFillePrevisualisation previsuFille_;

  protected FudaaProjetInformationsFrame fProprietes_;

  protected DimducFilleGraphe fGrapheCM_ = null, fGraphePED_ = null;

  protected DimducTableauResultats table_ = null;

  protected DimducCourbeMoments cml_ = null, cmc_ = null;

  protected DimducCourbeEDL cedl_ = null;

  protected DimducCourbeEDC cedc_ = null;

  protected BuAssistant assistant_;

  protected BuHelpFrame aide_;

  protected BuTaskView taches_; // Pointe

  // vers
  // resultats()
  // de
  // SERVEUR_DIMDUC
  private IConnexion connexionDimduc_;

  private boolean resultatsDispo_;
  static {
    DIMDUC_SOFT.name = "Dimduc";
    DIMDUC_SOFT.version = "1.11";
    DIMDUC_SOFT.date = "2006-02-01";
    DIMDUC_SOFT.rights = "Tous droits r�serv�s. CETMEF (c)2000";
    DIMDUC_SOFT.contact = "sebastien.hanrard@cetmef.equipement.gouv.fr";
    DIMDUC_SOFT.license = "GPL2";
    DIMDUC_SOFT.languages = "fr";
    DIMDUC_SOFT.logo = DimducResource.DIMDUC.getIcon("dimduc_logo");
    DIMDUC_SOFT.banner = DimducResource.DIMDUC.getIcon("dimduc_banner");
    DIMDUC_SOFT.http = "http://www.cetmef.equipement.gouv.fr/logiciels/visu_logiciels_tous.php";
    DIMDUC_SOFT.update = "http://www.cetmef.equipement.gouv.fr/logiciels/visu_logiciels_tous.php";
    DIMDUC_SOFT.authors = new String[] { "Sebastien Hanrard, Christian Barou " };
    DIMDUC_SOFT.contributors = new String[] { "Equipes Dodico, Ebli et Fudaa" };
    DIMDUC_SOFT.documentors = new String[] { "Sebastien Hanrard" };
    DIMDUC_SOFT.testers = new String[] { "Christian Barou" };
    DIMDUC_SOFT.specifiers = new String[] { "Sebastien Hanrard" };

  }

  /**
   * Constructeur de l objet DimducImplementation.
   */
  public DimducImplementation() {
    super();
    idDimduc_.name = "Etude";
    idDimduc_.version = "1.11";
    idDimduc_.organization = "CETMEF";
    idDimduc_.author = System.getProperty("user.name");
    idDimduc_.contact = idDimduc_.author + "@cetmef.equipement.gouv.fr";
    idDimduc_.date = FuLib.date();
    idDimduc_.logo = EbliResource.EBLI.getIcon("dimduc_icon.gif");
    BuPrinter.INFO_LOG = DIMDUC_SOFT;
    BuPrinter.INFO_DOC = idDimduc_;
  }

  public static BuInformationsSoftware informationsSoftware() {
    return DIMDUC_SOFT;
  }

  public BuPreferences getApplicationPreferences() {
    return DimducPreferences.DIMDUC;
  }

  public FudaaAstucesAbstract getAstuces() {
    return DimducAstuces.DIMDUC;
  }

  /**
   * Accede a l attribut InformationsSoftware de DimducImplementation object.
   *
   * @return valeur InformationsSoftware
   */
  public BuInformationsSoftware getInformationsSoftware() {
    return DIMDUC_SOFT;
  }

  /**
   * Accede a l attribut InformationsDocument de DimducImplementation object.
   *
   * @return valeur InformationsDocument
   */
  public BuInformationsDocument getInformationsDocument() {
    return idDimduc_;
  }

  public void init() {
    super.init();
    aide_ = null;
    try {
      setTitle(DIMDUC_SOFT.name + " " + DIMDUC_SOFT.version);
      // BuMenuBar mb = BuMenuBar.buildBasicMenuBar();
      final BuMenuBar mb = getMainMenuBar();
      // setMainMenuBar(mb);
      // mb.addActionListener(this);
      // mb.addMenu( construitMenuEdition (IS_APPLICATION) );
      mb.addMenu(construitMenuDonnees(IS_APPLICATION));
      mb.addMenu(construitMenuCalculs(IS_APPLICATION));
      mb.addMenu(construitMenuResultats(IS_APPLICATION));
      ((BuMenu) mb.getMenu("MENU_EDITION")).addMenuItem("Console", "CONSOLE", false);
      final BuToolBar tb = getMainToolBar(); // .buildBasicToolBar();
      tb.addSeparator();
      // barre de boutons personnalis�e :
      /*
       * tb.addToolButton("Creer", "CREER", true); tb.addToolButton("Ouvrir", "OUVRIR", true);
       * tb.addToolButton("Enregistrer", "ENREGISTRER", false); tb.addToolButton("Fermer ", "FERMER", false);
       * tb.addToolButton("Imprimer", "IMPRIMER", false);
       */
      tb.addToolButton("Calculer", "CALCULER", false);
      tb.addToolButton("Connecter", "CONNECTER", FudaaResource.FUDAA.getIcon("connecter"), true);
      // activation au d�marrage de Items ouvrir ou creer :
      setEnabledForAction("CREER", true);
      setEnabledForAction("OUVRIR", true);
      setEnabledForAction("REOUVRIR", true);
      setEnabledForAction("ENREGISTRER", false);
      setEnabledForAction("ENREGISTRERSOUS", false);
      setEnabledForAction("PROPRIETE", false);
      setEnabledForAction("FERMER", false);
      setEnabledForAction("QUITTER", true);
      setEnabledForAction("EXPORTER", false);
      setEnabledForAction("IMPRIMER", false);
      setEnabledForAction("CALCULER", false);
      setEnabledForAction("PREFERENCE", true);
      setEnabledForAction("AIDE_INDEX", true);
      setEnabledForAction("AIDE_ASSISTANT", true);
      final BuMenuRecentFiles mr = (BuMenuRecentFiles) mb.getMenu("REOUVRIR");
      if (mr != null) {
        mr.setPreferences(DimducPreferences.DIMDUC);
        mr.setResource(DimducResource.DIMDUC);
        mr.setEnabled(true);
      }
      assistant_ = new DimducAssistant();
      taches_ = new BuTaskView();
      fp_ = new DimducFilleParametres(getApp());
      premiereFoisParametres_ = true;
      // panel
      final BuScrollPane sp = new BuScrollPane(taches_);
      sp.setPreferredSize(new Dimension(150, 80));
      final BuMainPanel mp = getMainPanel();
      mp.getRightColumn().addToggledComponent("Assistant", "ASSISTANT", assistant_, this);
      mp.getRightColumn().addToggledComponent(BuResource.BU.getString("T�ches"), "TACHE", sp, this);
      mp.setLogo(DIMDUC_SOFT.logo);
      mp.setAssistant(assistant_);
      mp.setTaskView(taches_);
    } catch (final Throwable t) {
      System.err.println("$$$ " + t);
      t.printStackTrace();
    }
  }

  // fin void init
  // void start :
  public void start() {
    super.start();
    if (DimducPreferences.DIMDUC.getBooleanProperty("first.time", true)) {
      about();
      DimducPreferences.DIMDUC.putBooleanProperty("first.time", false);
      DimducPreferences.DIMDUC.writeIniFile();
    }
    assistant_.changeAttitude(BuAssistant.PAROLE, "Bienvenue !\n" + DIMDUC_SOFT.name + " " + DIMDUC_SOFT.version);

    final BuMainPanel mp = getMainPanel();
    // BuColumn rc = mp.getRightColumn();
    projet_ = new FudaaProjet(getApp(), new FudaaFiltreFichier("dimduc"));
    fProprietes_ = new FudaaProjetInformationsFrame(this);
    fProprietes_.setProjet(projet_);
    mp.doLayout();
    mp.validate();
    assistant_.addEmitters((Container) getApp());
    assistant_.changeAttitude(BuAssistant.ATTENTE, "Vous pouvez cr�er un\nnouveau projet Dimduc\nou en ouvrir un");
    // Application des pr�f�rences
    BuPreferences.BU.applyOn(this);
    // Preferences Dimduc
    DimducPreferences.DIMDUC.applyOn(this);
    if (FudaaAidePreferencesPanel.isAstucesVisibles(getApplicationPreferences())) {
      FudaaAstucesDialog.showDialog(getApplicationPreferences(), this, getAstuces());
    }
  }

  // Actions :
  public void actionPerformed(final ActionEvent _evt) {
    String action = _evt.getActionCommand();
    System.err.println("ACTION=" + action);
    final int i = action.indexOf('(');
    String arg = null;
    if (i >= 0) {
      arg = action.substring(i + 1, action.length() - 1);
      action = action.substring(0, i);
    }
    if (action.equals("CREER")) {
      creer();
    } else if (action.equals("OUVRIR")) {
      ouvrir();
    } else if (action.equals("REOUVRIR")) {
      ouvrir(arg);
    } else if (action.equals("ENREGISTRER")) {
      enregistrer();
    } else if (action.equals("ENREGISTRERSOUS")) {
      enregistrerSous();
    } else if (action.equals("FERMER")) {
      fermer();
      // } else if (action.equals("CONSOLE")) {
      // ts_.show();
      // } else if (action.equals("PREFERENCE")) {
      // preferences();
    } else if (action.equals("PARAMETRES")) {
      parametres();
    } else if (action.equals("VALIDER")) {
      valider();
    } else if (action.equals("CALCULER")) {
      calculer();
    } else if (action.equals("PANNEAU LEGER")) {
      panneauLeger();
    } else if (action.equals("TABLEAU MOMENTS LEGER")) {
      tableauMomentsLeger();
    } else if (action.equals("COURBE MOMENTS LEGER")) {
      courbeMomentsLeger();
    } else if (action.equals("COURBED LEGER")) {
      pEDL();
    } else if (action.equals("PANNEAU COURT")) {
      panneauCourt();
    } else if (action.equals("TABLEAU MOMENTS COURT")) {
      tableauMomentsCourt();
    } else if (action.equals("COURBE MOMENTS COURT")) {
      courbeMomentsCourt();
    } else if (action.equals("COURBED COURT")) {
      pEDC();
    } else if (action.equals("TOUS")) {
      tousLesDucs();
    } else if (action.equals("PROPRIETE")) {
      if (fProprietes_.getDesktopPane() != getMainPanel().getDesktop()) {
        addInternalFrame(fProprietes_);
      } else {
        activateInternalFrame(fProprietes_);
      }
    } else if (action.equals("ASSISTANT") || action.equals("CALQUE")) {
      final BuColumn rc = getMainPanel().getRightColumn();
      rc.toggleComponent(action);
      setCheckedForAction(action, rc.isToggleComponentVisible(action));
      // } else if (("PREVISUALISER".equals(action)) ||
      // ("MISEENPAGE".equals(action)) || ("IMPRIMER".equals(action))) {
      // gestionnaireImpression(action);
      // } else if (action.equals("ASTUCE")) {
      // FudaaAstucesDialog.showDialog(getApplicationPreferences(), this,
      // getAstuces());
    } else {
      super.actionPerformed(_evt);
    }
  }

  public void contextHelp(final String _url) {
    String url = _url;
    if ((url == null) || (url.length() == 0) || (url.startsWith("#"))) {
      url = "index.html";
    }
    super.contextHelp(url);
  }

  public void displayURL(String _url) {
    if ((_url == null) || (_url.length() == 0)) {
      _url = FudaaLib.LOCAL_MAN;
    }
    if ((_url.startsWith("file")) && (_url.endsWith("/"))) {
      _url = _url + "index.html";
    }
    System.out.println(_url);
    FudaaBrowserControl.displayURL(_url);
  }

  /**
   * Impression de la page <code>_target</code> dans un nouveau thread.
   */
  public void cmdImprimer(final EbliPageable _target) {
    final PrinterJob printJob = PrinterJob.getPrinterJob();
    final BuMainPanel mp = getMainPanel();
    printJob.setPageable(_target);
    if (printJob.printDialog()) {
      mp.setProgression(5);
      new BuTaskOperation(this, BuResource.BU.getString("Impression")) {

        public void act() {
          try {
            mp.setProgression(10);
            printJob.print();
            mp.setProgression(100);
          } catch (final Exception _ex) {
            mp.setProgression(0);
            _ex.printStackTrace();
          }
        }
      }.start();
    }
  }

  public void cmdMiseEnPage(final EbliPageable _target) {
    new EbliMiseEnPageDialog(_target, getApp(), getInformationsSoftware()).activate();
  }

  public void cmdPrevisualisation(final EbliPageable _target) {
    if (previsuFille_ == null) {
      previsuFille_ = new EbliFillePrevisualisation(getApp(), _target);
      addInternalFrame(previsuFille_);
    } else {
      previsuFille_.setEbliPageable(_target);
      if (previsuFille_.isClosed()) {
        addInternalFrame(previsuFille_);
      } else {
        activateInternalFrame(previsuFille_);
      }
    }
    try {
      previsuFille_.setMaximum(true);
    } catch (final java.beans.PropertyVetoException _e) {
      previsuFille_.setSize(100, 100);
    }
  }

  public void oprServeurCopie() {
    System.err.println("Lancement du serveur de copie d'ecran");
    new ServeurCopieEcran(getMainPanel(), "ScreenSpy");
  }

  public String oprCalculer() {
    setEnabledForAction("CALCULER", false);
    final BuMainPanel mp = getMainPanel();
    if (Fu.DEBUG) {
      FuLog.debug("FDI:Transmission des param�tres...");
    }
    mp.setMessage("Transmission des param�tres...");
    mp.setProgression(0);
    dimducParams_ = IParametresDimducHelper.narrow(serveurDimduc_.parametres(connexionDimduc_));
    dimducParams_.parametres01((SParametres01) projet_.getParam(DimducResource.DIMDUC01));
    dimducParams_.parametres02((SParametres02) projet_.getParam(DimducResource.DIMDUC02));
    if (Fu.DEBUG) {
      FuLog.debug("Execution du calcul...");
    }
    mp.setMessage("Execution du calcul...");
    mp.setProgression(20);
    String err = null;
    try {
      serveurDimduc_.calcul(connexionDimduc_);
      dimducResults_ = IResultatsDimducHelper.narrow(serveurDimduc_.resultats(connexionDimduc_));
      err = dimducResults_.erreur();
    } catch (final org.omg.CORBA.UNKNOWN u) {
      FuLog.error(u);
      return u.getMessage();
    }
    return err;

  } // fin void oprCalculer

  public void oprReceptionResultats() {
    receptionResultats(false);
  }

  protected BuMenu construitMenuDonnees(final boolean _app) {
    final BuMenu d = new BuMenu("Donn�es", "DONNEES");
    d.addMenuItem("Entrer", "PARAMETRES", false);
    d.addSeparator("");
    d.addMenuItem("Valider", "VALIDER", false);
    return d;
  }

  /**
   * Menu Calcul : specifique DIMDUC.
   *
   * @param _app
   */
  protected BuMenu construitMenuCalculs(final boolean _app) {
    final BuMenu c = new BuMenu("Calcul", "CALCUL");
    c.addMenuItem("Lancer", "CALCULER", false);
    return c;
  }

  /**
   * Menu donnees : specifique DIMDUC.
   *
   * @param _app
   */
  protected BuMenu construitMenuResultats(final boolean _app) {
    final BuMenu r = new BuMenu("R�sultats", "RESULTATS");
    r.addSeparator("Consulter");
    r.addSeparator("Duc le plus l�ger");
    r.addMenuItem("Caract�ristiques (panneau)", "PANNEAU LEGER", false);
    r.addMenuItem("Tableau des moments ", "TABLEAU MOMENTS LEGER", false);
    r.addMenuItem("Courbe  des moments ", "COURBE MOMENTS LEGER", false);
    r.addMenuItem("Courbe ED D�fense", "COURBED LEGER", false);
    r.addSeparator("Duc le plus court");
    r.addMenuItem("Caract�ristiques (panneau)", "PANNEAU COURT", false);
    r.addMenuItem("Tableau des moments ", "TABLEAU MOMENTS COURT", false);
    r.addMenuItem("Courbe  des moments ", "COURBE MOMENTS COURT", false);
    r.addMenuItem("Courbe ED D�fense", "COURBED COURT", false);
    r.addSeparator("Les deux ducs");
    r.addMenuItem("Caract�ristiques (tableau)", "TOUS", false);
    return r;
  }

  /**
   * Commandes activ�es d�s qu'une etude est charg�e (dans creer et ouvrir).
   */
  protected void activerCommandesEtude() {
    setEnabledForAction("PARAMETRES", true);
    setEnabledForAction("ENREGISTRER", true);
    setEnabledForAction("ENREGISTRERSOUS", true);
    setEnabledForAction("FERMER", true);
    setEnabledForAction("PROPRIETE", true);
  }

  /**
   * Creer.
   */
  protected void creer() {
    if (!projet_.estVierge()) {
      fermer();
    }
    projet_.creer();
    if (!projet_.estConfigure()) {
      projet_.fermer();
    } // si creation de projet annulee
    else {
      // nouveau projet cree
      if (fp_ != null) {
        fp_.updatePanels(projet_);
        parametres();
      }

      activerCommandesEtude();
      setTitle(projet_.getFichier());
    } // / fin else if(!projet ...)
  }

  protected void ouvrir() {
    if (!projet_.estVierge()) {
      fermer();
    }
    projet_.ouvrir();
    if (!projet_.estConfigure()) {
      // si ouverture de projet echouee
      projet_.fermer();
      projectClosed();
    } else {
      if (fp_ != null) {
        fp_.updatePanels(projet_);
        parametres();
      }
      new BuDialogMessage(getApp(), DIMDUC_SOFT, "Param�tres charg�s").activate();
      activerCommandesEtude();
      setTitle(projet_.getFichier());
    } // fin else
  }

  private void projectClosed() {
    setEnabledForAction("PARAMETRES", false);
    setEnabledForAction("VALIDER", false);
    setEnabledForAction("FERMER", false);
    setEnabledForAction("ENREGISTRER", false);
    setEnabledForAction("ENREGISTRERSOUS", false);
    setEnabledForAction("CALCULER", false);
  }

  protected void ouvrir(final String _arg) {
    if (!projet_.estVierge()) {
      fermer();
    }
    projet_.ouvrir(_arg);
    if (!projet_.estConfigure()) {
      projet_.fermer(); // si ouverture de projet echouee
      projectClosed();
    } else {
      if (fp_ != null) {
        fp_.updatePanels(projet_);
        parametres();

      }
      new BuDialogMessage(getApp(), DIMDUC_SOFT, "Param�tres charg�s").activate();
      activerCommandesEtude();
      setTitle(projet_.getFichier());
      if (_arg == null) {
        final String r = projet_.getFichier();
        getMainMenuBar().addRecentFile(r, "dimduc");
        DimducPreferences.DIMDUC.writeIniFile();
      }
    }
  }

  protected void enregistrer() {
    if (fp_ != null) {
      saveParameters();
    } // fin if (fp_ != null)
    else {
      projet_.enregistre();
    }
  } // fin void enregistrer

  private void saveParameters() {
    final SParametres01 params01 = fp_.getTextes01();
    final SParametres02 params02 = fp_.getTextes02();
    if (params01 == null) {
      new BuDialogError(getApp(), DIMDUC_SOFT, "parametres 01 => \nun champ est vide ou de format incorrect ! ")
          .activate();
    } else if (params02 == null) {
      new BuDialogError(getApp(), DIMDUC_SOFT, "parametres 02 => \nun champ est vide ou de format incorrect ! ")
          .activate();
    } else {
      projet_.addParam(DimducResource.DIMDUC01, params01);
      projet_.addParam(DimducResource.DIMDUC02, params02);
      projet_.enregistre();
    }
  }

  private void saveLastProjectPath() {
    boolean found = false;
    final String r = projet_.getFichier();
    for (int i = 1; i < RECENT_COUNT; i++) {
      if (r.equals(DimducPreferences.DIMDUC.getStringProperty("file.recent." + i + ".path"))) {
        found = true;
      }
    }
    if (!found) {
      getMainMenuBar().addRecentFile(r, "dimduc");
      DimducPreferences.DIMDUC.writeIniFile();
    }
    setEnabledForAction("REOUVRIR", true);
  }

  protected void enregistrerSous() {
    if (fp_ != null) {
      saveLastProjectPath();
      saveParameters();
    } // fin if fp_ != null
    else {
      projet_.enregistreSous();
    }
    setTitle(projet_.getFichier());
  }

  // void fermer
  protected void fermer() {
    final BuDesktop dk = getMainPanel().getDesktop();
    if (fProprietes_.isVisible()) {
      fProprietes_.setVisible(false);
    }
    // fermeture projet :
    if (fp_ != null) {
      try {
        saveLastProjectPath();
        fp_.setClosed(true);
        fp_.setSelected(false);
        if (new BuDialogConfirmation(getApp(), DIMDUC_SOFT, "Voulez-vous enregistrer votre projet ?").activate() == JOptionPane.YES_OPTION) {
          enregistrer();
        }
      } catch (final PropertyVetoException e) {}
      dk.removeInternalFrame(fp_);
      // fp_ = null;
    } // fin if(fp_!=null)
    removeInternalFrames(getAllInternalFrames());
    pdc_ = null;
    pdl_ = null;
    fGraphePED_ = null;
    fGrapheCM_ = null;
    fTableau_ = null;
    table_ = null;
    resultatsML_ = null;
    resultatsMC_ = null;
    resultatsDL_ = null;
    resultatsDC_ = null;
    resultatsTS_ = null;
    // fin if (pdc_!=null)
    // dimducResults_=null;
    resultatsDispo_ = false;
    projet_.fermer();
    setEnabledForAction("PARAMETRES", false);
    setEnabledForAction("VALIDER", false);
    setEnabledForAction("FERMER", false);
    setEnabledForAction("PROPRIETE", false);
    setEnabledForAction("ENREGISTRER", false);
    setEnabledForAction("ENREGISTRERSOUS", false);
    setEnabledForAction("CALCULER", false);
    setEnabledForAction("PANNEAU LEGER", false);
    setEnabledForAction("TABLEAU MOMENTS LEGER", false);
    setEnabledForAction("COURBE MOMENTS LEGER", false);
    setEnabledForAction("COURBED LEGER", false);
    setEnabledForAction("PANNEAU COURT", false);
    setEnabledForAction("TABLEAU MOMENTS COURT", false);
    setEnabledForAction("COURBE MOMENTS COURT", false);
    setEnabledForAction("COURBED COURT", false);
    setEnabledForAction("TOUS", false);
    setTitle("DIMDUC");
  }

  // void preferences :
  protected void buildPreferences(final List _prefs) {
    _prefs.add(new BuUserPreferencesPanel(this));
    _prefs.add(new BuLanguagePreferencesPanel(this));
    _prefs.add(new BuDesktopPreferencesPanel(this));
    _prefs.add(new BuLookPreferencesPanel(this));
    _prefs.add(new BuBrowserPreferencesPanel(this));
    _prefs.add(new EbliMiseEnPagePreferencesPanel());
    _prefs.add(new FudaaAidePreferencesPanel(this, getApplicationPreferences()));
  }

  // void parametres :
  protected void parametres() {
    if (premiereFoisParametres_) {
      addInternalFrame(fp_);
      premiereFoisParametres_ = false;
    } else {
      if (fp_.isClosed()) {
        addInternalFrame(fp_);
      } else {
        activateInternalFrame(fp_);
      }
    }
    if (!projet_.estVierge()) {
      fp_.updatePanels(projet_);
    }
    assistant_.addEmitters(fp_);
    setEnabledForAction("VALIDER", true);
    setEnabledForAction("CALCULER", false);
  } // fin void parametres

  // void valider : appelle classe qui r�capitule les contr�les de validit� des
  // donn�es :
  protected void valider() {
    dv_ = new DimducValidation(fp_);
    dv_.validerDonneesProjet();
    if (dv_.valide()) {
      setEnabledForAction("CALCULER", true);
      new BuDialogMessage(getApp(), DIMDUC_SOFT, "Vos donn�es sont valides.\n Vous pouvez lancer le calcul").activate();
    } // fin if fp_.valide == true
    else if (!dv_.valide()) {
      new BuDialogMessage(getApp(), DIMDUC_SOFT,
          "Vos donn�es ne sont pas valides.\n Vous ne pouvez pas lancer de calcul. \nVeuillez corriger : \n "
              + dv_.mesErreurs()).activate();
      setEnabledForAction("CALCULER", false);
    } // fin else
  }

  // panneau des caract�ristiques du duc le plus leger
  protected void panneauLeger() {
    if (resultatsDL_ == null) {
      // creation resultatsDL :
      resultatsDL_ = (SSollicitationAccostage[]) projet_.getResult(DimducResource.DIMDUC05);
    }
    if (resultatsDL_ == null) {
      return;
    }
    if (pdl_ == null) {
      // creation du classeur onglets :
      pdl_ = new DimducPanneauDuc(resultatsDL_, "Duc le plus l�ger", 0);
      // ouverture du classeur onglets :
      addInternalFrame(pdl_);
      pdl_.pack();
    } // fin if (pdl == null)
    if (pdl_.isClosed()) {
      // ouverture du classeur a onglets
      addInternalFrame(pdl_);
      pdl_.pack();
    } // fin if (pdl_.isClosed())
    assistant_.addEmitters(pdl_);
    activateInternalFrame(pdl_);
  } // fin void panneauLeger ()

  // tableau des moments duc le plus leger
  protected void tableauMomentsLeger() {
    if (fTableau_ == null) {
      // creation fTableau :
      fTableau_ = new FudaaFilleTableau(this, getInformationsDocument());
      table_ = new DimducTableauResultats();
      table_.setTableHeader(new CtuluTableColumnHeader(table_.getColumnModel()));
      fTableau_.setTable(table_);
      fTableau_.setBackground(Color.white);
      fTableau_.setPreferredSize(new Dimension(640, 480));
      // ouverture fTableau :
      addInternalFrame(fTableau_);
      fTableau_.pack();
    } // fin if(fTableau_== null)
    if (resultatsML_ == null) {
      // creation resultatsML_ :
      resultatsML_ = (STableauMoments[]) projet_.getResult(DimducResource.DIMDUC03);
    }
    if (resultatsML_ == null) {
      return;
    }
    if (fTableau_.isClosed()) {
      // ouverture du classeur a onglets
      addInternalFrame(fTableau_);
      fTableau_.pack();
    } // fin if (fTableau_.isClosed())
    fTableau_.setTitle("Duc le plus l�ger: tableau des moments");
    table_.setResultatsML(resultatsML_);
    table_.setMode(3);
    assistant_.addEmitters(fTableau_);
    activateInternalFrame(fTableau_);
  } // fin void tableauMomentsLeger

  // courbe des moments duc le plus leger
  protected void courbeMomentsLeger() {
    if (fGrapheCM_ == null) {
      // creation fGraphe_CM_ :
      fGrapheCM_ = new DimducFilleGraphe(this, getInformationsDocument());
      fGrapheCM_.setBackground(Color.white);
      fGrapheCM_.setSize(640, 480);
      fGrapheCM_.setBounds(50, 50, 50, 50); // marges
      // ouverture fGraphe_CM_ :
      fGrapheCM_.pack();
      addInternalFrame(fGrapheCM_);
    } // fin if(fGraphe_CM_== null)
    if (resultatsML_ == null) {
      // creation resultatsML_ :
      resultatsML_ = (STableauMoments[]) projet_.getResult(DimducResource.DIMDUC03);
    }
    if (resultatsML_ == null) {
      return;
    }
    if (fGrapheCM_.isClosed()) {
      // ouverture du classeur a onglets
      addInternalFrame(fGrapheCM_);
      fGrapheCM_.pack();
    } // fin if (fGraphe_CM_.isClosed())
    // creation courbe cml_ :
    cml_ = new DimducCourbeMoments(resultatsML_, "Ouvrage le plus l�ger", "Moment", "Cote", UNITE_EFFORT, "m");
    fGrapheCM_.setVisible(true);
    fGrapheCM_.setTitle("Ouvrage le plus l�ger");
    fGrapheCM_.setGraphe(cml_);
    activateInternalFrame(fGrapheCM_);
  } // fin void courbeMomentsLeger ()

  // Courbe ED D�fense duc le plus leger
  protected void pEDL() {
    if (fGraphePED_ == null) {
      // creation fGraphe_PED :
      fGraphePED_ = new DimducFilleGraphe(this, getInformationsDocument());
      fGraphePED_.setBackground(Color.white);
      fGraphePED_.setSize(640, 480);
      fGraphePED_.setBounds(100, 100, 100, 100);
      // ouverture fGraphe_PED_ :
      addInternalFrame(fGraphePED_);
      fGraphePED_.pack();
    } // fin if(fGraphe_PED_== null)
    if (resultatsDL_ == null) {
      // creation resultatsDL_ :
      resultatsDL_ = (SSollicitationAccostage[]) projet_.getResult(DimducResource.DIMDUC05);
    } // fin if(resultatsDL_==null)
    if (resultatsDL_ == null) {
      return;
    }
    if (fGraphePED_.isClosed()) {
      // ouverture du classeur a onglets
      addInternalFrame(fGraphePED_);
      fGraphePED_.pack();
    } // fin if (fGraphe_PED_.isClosed())
    // creation cedl_ :
    cedl_ = new DimducCourbeEDL(resultatsDL_, "Courbe ED d�fense : duc le plus l�ger", "Deformation", "Effort", "%",
        UNITE_EFFORT);
    fGraphePED_.setVisible(true);
    fGraphePED_.setGraphe(cedl_);
    fGraphePED_.setTitle("Courbe ED d�fense : duc le plus l�ger");

    activateInternalFrame(fGraphePED_);
  } // fin void pEDL()

  // panneau des caract�ristiques du duc le plus court
  protected void panneauCourt() {
    if (resultatsDC_ == null) {
      resultatsDC_ = (SSollicitationAccostage[]) projet_.getResult(DimducResource.DIMDUC05);
    }
    if (resultatsDC_ == null) {
      return;
    }
    if (pdc_ == null) {
      // creation du classeur a onglets
      pdc_ = new DimducPanneauDuc(resultatsDC_, "Duc le plus court", 1);
      // ouverture du classeur a onglets
      addInternalFrame(pdc_);
      pdc_.pack();
    } // fin if (pdc_ == null)
    if (pdc_.isClosed()) {
      // ouverture du classeur a onglets
      addInternalFrame(pdc_);
      pdc_.pack();
    } // fin if (pdc_.isClosed())
    assistant_.addEmitters(pdc_);
    activateInternalFrame(pdc_);
  } // fin void panneauCourt()

  // tableau des moments duc le pluc court
  protected void tableauMomentsCourt() {
    if (fTableau_ == null) {
      // creation fTableau_ :
      fTableau_ = new FudaaFilleTableau(this, getInformationsDocument());
      table_ = new DimducTableauResultats();
      fTableau_.setTable(table_);
      fTableau_.setBackground(Color.white);
      fTableau_.setPreferredSize(new Dimension(640, 480));
      // ouverture fTableau_ :
      addInternalFrame(fTableau_);
      fTableau_.pack();
    } // fin if(fTableau_== null)
    if (fTableau_.isClosed()) {
      // ouverture du classeur a onglets
      addInternalFrame(fTableau_);
      fTableau_.pack();
    } // fin if (fTableau_.isClosed())
    if (resultatsMC_ == null) {
      // creation resultatsMC_ :
      resultatsMC_ = (STableauMoments[]) projet_.getResult(DimducResource.DIMDUC04);
    }
    if (resultatsMC_ == null) {
      return;
    }
    fTableau_.setTitle("Duc le plus court: tableau des moments");
    table_.setResultatsMC(resultatsMC_);
    table_.setMode(4);
    activateInternalFrame(fTableau_);
  } // fin void tableauMomentsCourt ()

  // graphe des moments duc le plus court
  protected void courbeMomentsCourt() {
    if (fGrapheCM_ == null) {
      // creation fGraphe_CM_ :
      fGrapheCM_ = new DimducFilleGraphe(this, getInformationsDocument());
      fGrapheCM_.setBackground(Color.white);
      fGrapheCM_.setSize(640, 480);
      fGrapheCM_.setBounds(50, 50, 50, 50);
      // ouverture fGraphe_CM_ :
      addInternalFrame(fGrapheCM_);
      fGrapheCM_.pack();
    } // fin if(fGraphe_CM_== null)
    if (resultatsMC_ == null) {
      // creation resultatsMC_ :
      resultatsMC_ = (STableauMoments[]) projet_.getResult(DimducResource.DIMDUC04);
    }
    if (resultatsMC_ == null) {
      return;
    }
    if (fGrapheCM_.isClosed()) {
      // ouverture du classeur a onglets
      addInternalFrame(fGrapheCM_);
      fGrapheCM_.pack();
    } // fin if (fGraphe_CM_.isClosed())
    // creation cmc_ :
    cmc_ = new DimducCourbeMoments(resultatsMC_, "Ouvrage le plus court", "Moment", "Cote", UNITE_EFFORT, "m");
    // affichage :
    fGrapheCM_.setVisible(true);
    fGrapheCM_.setTitle("Ouvrage le plus court");
    fGrapheCM_.setGraphe(cmc_);
    activateInternalFrame(fGrapheCM_);
  } // fin void courbeMomentsCourt ()

  // Courbe ED D�fense graphe le plus court
  protected void pEDC() {
    if (fGraphePED_ == null) {
      // creation fGraphe_PED_ :
      fGraphePED_ = new DimducFilleGraphe(this, getInformationsDocument());
      fGraphePED_.setBackground(Color.white);
      fGraphePED_.setSize(640, 480);
      fGraphePED_.setBounds(100, 100, 100, 100);
      // ouverture fGraphe_PED_ :
      addInternalFrame(fGraphePED_);
      fGraphePED_.pack();
    } // fin if(fGraphe_PED_== null)
    if (resultatsDC_ == null) {
      // creation resultatsDC_ :
      resultatsDC_ = (SSollicitationAccostage[]) projet_.getResult(DimducResource.DIMDUC05);
    }
    if (resultatsDC_ == null) {
      return;
    }
    if (fGraphePED_.isClosed()) {
      // ouverture du classeur a onglets
      addInternalFrame(fGraphePED_);
      fGraphePED_.pack();
    } // fin if (fGraphe_PED_.isClosed())
    // creation cedc_ :
    cedc_ = new DimducCourbeEDC(resultatsDC_, "Courbe ED D�fense : duc le plus court", "Deformation", "Effort", "%",
        UNITE_EFFORT);
    // affichage :
    fGraphePED_.setVisible(true);
    fGraphePED_.setGraphe(cedc_);
    fGraphePED_.setTitle("Courbe ED D�fense : duc le plus court");

    activateInternalFrame(fGraphePED_);
  } // fin void pEDC()

  // tableau des ducs convenant : pour l'instant le plus court et le plus leger
  protected void tousLesDucs() {
    if (fTableau_ == null) {
      // creation fTableau_ :
      fTableau_ = new FudaaFilleTableau(this, getInformationsDocument());
      table_ = new DimducTableauResultats();
      fTableau_.setTable(table_);
      fTableau_.setBackground(Color.white);
      fTableau_.setPreferredSize(new Dimension(640, 480));
      // ouverture fTableau_ :
      addInternalFrame(fTableau_);
      fTableau_.pack();
    } // fin if(fTableau_== null)
    if (resultatsTS_ == null) {
      // creation resultatsTS_ :
      resultatsTS_ = (SSollicitationAccostage[]) projet_.getResult(DimducResource.DIMDUC05);
    }
    if (resultatsTS_ == null) {
      return;
    }
    if (fTableau_.isClosed()) {
      // ouverture du classeur a onglets
      addInternalFrame(fTableau_);
      fTableau_.pack();
    } // fin if (fTableau_.isClosed())
    fTableau_.setTitle("Tous les ducs");
    table_.setResultatsTS(resultatsTS_);
    table_.setMode(2);
    assistant_.addEmitters(fTableau_);
    activateInternalFrame(fTableau_);
  } // fin void tousLesducs ()

  /**
   * @return true si une fenetre de r�sultats est ouverte.
   */
  boolean isResultsFrameOpen() {
    return isOpen(fTableau_) || isOpen(pdc_) || isOpen(pdl_) || isOpen(fGrapheCM_) || isOpen(fGraphePED_);
  }

  void closeResultsFrame() {
    removeInternalFrames(new JInternalFrame[] { fTableau_, pdc_, pdl_, fGrapheCM_, fGraphePED_ });
  }

  static boolean isOpen(final JInternalFrame _f) {
    return _f != null && !_f.isClosed();
  }

  public boolean confirmExit() {
    final boolean r=super.confirmExit();
    if(r) {
      fermer();
    }
    return r;
  }

  protected void calculer() {
    if (isResultsFrameOpen()) {
      if (!question("Calcul lanc�", "Toutes les fen�tres de r�sultats " +
          "vont �tre ferm�es.\nVoulez-vous continuer ?")) {
        return;
      }
      closeResultsFrame();
    }
    // on intialise les variables temporaires.
    resultatsDispo_ = false;
    resultatsML_ = null;
    resultatsMC_ = null;
    resultatsDL_ = null;
    resultatsDC_ = null;
    resultatsTS_ = null;
    pdl_ = null;
    pdc_ = null;
    if (!isConnected()) {
      new BuDialogError(getApp(), DIMDUC_SOFT, "vous n'etes pas connect� � un serveur!").activate();
      return;
    }
    if (fp_ != null) {
      new BuTaskOperation(this, "Calcul") {
        public void act() {
          final SParametres01 params01 = fp_.getTextes01();
          final SParametres02 params02 = fp_.getTextes02();
          if (params01 == null) {
            new BuDialogError(getApp(), DIMDUC_SOFT, "parametres 01 => \nun champ est vide ou de format incorrect ! ")
                .activate();
          } else if (params02 == null) {
            new BuDialogError(getApp(), DIMDUC_SOFT, "parametres 02 => \nun champ est vide ou de format incorrect ! ")
                .activate();
          } else {
            projet_.addParam(DimducResource.DIMDUC01, params01);
            projet_.addParam(DimducResource.DIMDUC02, params02);
            // le message d'erreur du calcul
            final String err = oprCalculer();
            // true si le calcul s'est bien d�roul�
            final boolean isOk = err == null;
            // on lance la mise � jour de l'interface dans le thread de swing
            BuLib.invokeNow(new Runnable() {

              public void run() {
                if (err == null) {
                  receptionResultats(true);
                } else {
                  DimducImplementation.this.error("calcul", err);
                }
                // on rend le bouton calcul inaccessible
                setEnabledForAction("CALCULER", false);
                if (fp_.nombre_courbesED_ > 0) {
                  setEnabledForAction("COURBED LEGER", isOk);
                  setEnabledForAction("COURBED COURT", isOk);
                }
                setEnabledForAction("TABLEAU MOMENTS LEGER", isOk);
                setEnabledForAction("PANNEAU LEGER", isOk);
                setEnabledForAction("COURBE MOMENTS LEGER", isOk);
                setEnabledForAction("PANNEAU COURT", isOk);
                setEnabledForAction("TABLEAU MOMENTS COURT", isOk);
                setEnabledForAction("COURBE MOMENTS COURT", isOk);
                setEnabledForAction("TOUS", isOk);
                setEnabledForAction("VALIDER", true);
                unsetMainMessage();
                unsetMainProgression();

              }

            });

          }
        }
      }.start();

    }// fin else
    // dimducResults_ =null;

  }

  /**
   * Reception resultats.
   *
   * @param _calcul
   */
  protected void receptionResultats(final boolean _calcul) {
    final BuMainPanel mp = getMainPanel();
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("FDI: Reception des resultats...");
    }
    mp.setMessage("Reception des resultats...");
    mp.setProgression(90);
    // if(dimducResults_==null)
    if (!resultatsDispo_) {
      // dimducResults_ =
      // IResultatsDimducHelper.narrow(SERVEUR_DIMDUC.resultats());
      projet_.addResult(DimducResource.DIMDUC03, dimducResults_.tableauMomentsLeger());
      projet_.addResult(DimducResource.DIMDUC04, dimducResults_.tableauMomentsCourt());
      projet_.addResult(DimducResource.DIMDUC05, dimducResults_.sollicitationsAccostage());
      resultatsDispo_ = true;
    }
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("FDI: Op�ration termin�e");
    }
    mp.setMessage("Operation terminee.");
    mp.setProgression(100);
    if (_calcul) {
      new BuDialogMessage(getApp(), DIMDUC_SOFT,
          "Calcul termin�\nCliquez sur \"Continuer\" pour voir les r�sultats � partir du menu 'R�sultats'.").activate();
    } else {
      new BuDialogMessage(getApp(), DIMDUC_SOFT, "R�sultats charg�s").activate();
    }
    unsetMainMessage();
    unsetMainProgression();
  }

  public boolean isCloseFrameMode() {
    return false;
  }

  protected void clearVariables() {
    connexionDimduc_ = null;
    serveurDimduc_ = null;
  }

  protected FudaaDodicoTacheConnexion[] getTacheConnexionMap() {
    return new FudaaDodicoTacheConnexion[] { new FudaaDodicoTacheConnexion(serveurDimduc_, connexionDimduc_) };
  }

  protected Class[] getTacheDelegateClass() {
    return new Class[] { DCalculDimduc.class };
  }

  protected void initConnexions(final Map _r) {
    final FudaaDodicoTacheConnexion c = (FudaaDodicoTacheConnexion) _r.get(DCalculDimduc.class);
    connexionDimduc_ = c.getConnexion();
    serveurDimduc_ = ICalculDimducHelper.narrow(c.getTache());
  }

} // fin classe class DimducImplementation
