/*
 * @file         DimducResource.java
 * @creation     2001-01-21
 * @modification $Date: 2006-09-19 15:02:09 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.dimduc;
import com.memoire.bu.BuResource;
/**
 * Le gestionnaire de ressources de Dimduc.
 *
 * @version      $Revision: 1.5 $ $Date: 2006-09-19 15:02:09 $ by $Author: deniger $
 * @author       Christian Barou 
 */
public class DimducResource extends BuResource {
  public final static String DIMDUC01= "01"; // dimducParams.parametres 01
  public final static String DIMDUC02= "02"; // dimducParams.parametres 02
  public final static String DIMDUC03= "03"; // STableauMoments resultatsML_ ,
  public final static String DIMDUC04= "04"; // STableauMoments resultatsMC_ ,
  public final static String DIMDUC05= "05";
  // SSollicitationAccostage :resultatsDC_,DL_,TS_
  public final static String DIMDUC06= "06";
  public final static String DIMDUC07= "07";
  public final static DimducResource DIMDUC= new DimducResource();
}
