/*
 * @file         DimducFilleParametres.java
 * @creation     2001-01-29
 * @modification $Date: 2006-09-19 15:02:08 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.dimduc;
import java.awt.BorderLayout;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.dimduc.SCoucheSol;
import org.fudaa.dodico.corba.dimduc.SParametres01;
import org.fudaa.dodico.corba.dimduc.SParametres02;
import org.fudaa.dodico.corba.ducalbe.SAccostage;
import org.fudaa.dodico.corba.ducalbe.SContrainteAdmissible;
import org.fudaa.dodico.corba.ducalbe.SCourbeEffortDeformation;
import org.fudaa.dodico.corba.ducalbe.SDefense;
import org.fudaa.dodico.corba.ducalbe.SEnsembleTubes;
import org.fudaa.dodico.corba.ducalbe.SPointEffortDeformation;
import org.fudaa.dodico.corba.ducalbe.STube;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
/**
 * Une fenetre fille pour entrer les parametres.
 *
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 15:02:08 $ by $Author: deniger $
 * @author       Christian Barou
 */
public class DimducFilleParametres
  extends JInternalFrame
  implements ItemListener, FocusListener {
  // declarations variables :
  protected static BuInformationsSoftware isDimduc_=
    new BuInformationsSoftware();
  protected DimducTextField it_;
  Object obj_newOption;
  // rajout pour test separation onglets :
  // DimducDonneesGeneralesParametres pnDonneesGenerales;
  // Ecran 01 : tete
  BuTextField tf_cote_tete, tf_fleche_tete;
  JComboBox jc_fleche_tete; // Oui/Non
  int nombre_fleches_= 0; // valeur par d�faut   0 ou 1
  double cote_tete_, fleche_tete_, fleche_tete_anc_; // enregistrement valeurs
  // Ecran 02 : tubes
  JComboBox jc_nb_tubes;
  int nombre_tubes_= 1; // valeur par d�faut 1 ou 2
  BuTextField tf_module_young, tf_entre_axes;
  BuTextField tf_diametre_mini, tf_diametre_maxi, tf_diametre_pas;
  double young_, entre_axes_;
  double diametre_mini_, diametre_maxi_, diametre_pas_;
  BuTextField tf_epaisseur_mini,
    tf_epaisseur_maxi,
    tf_epaisseur_pas,
    tf_epaisseur_cor;
  double epaisseur_mini_, epaisseur_maxi_, epaisseur_pas_, epaisseur_cor_;
  // contraintes
  JComboBox jc_nb_sigma;
  int isigma, nombre_contraintes_= 1;
  BuTextField tf_contrainte[]= new BuTextField[8]; // [8] : indexe de 0 a 7
  int contrainte_[]= new int[8];
  // Ecran 03 : sols
  JComboBox jc_nb_sols;
  int nombre_sols_= 1; // valeur par d�faut
  BuTextField tf_cote_sup_1, tf_cote_sup_2;
  double cote_sup_1_= 0;
  double cote_sup_2_= 0;
  BuTextField tf_butee_mini_1, tf_butee_mini_2;
  double butee_mini_1_, butee_mini_2_;
  BuTextField tf_cohesion_mini_1, tf_cohesion_mini_2;
  double cohesion_mini_1_, cohesion_mini_2_;
  BuTextField tf_poids_mini_1, tf_poids_mini_2;
  double poids_mini_1_, poids_mini_2_;
  // Ecran 04 : accostages
  JComboBox jc_nb_accostages;
  int iacc;
  int nombre_accostages_= 1;
  // cotes acc :
  BuTextField tf_cote_acc[]= new BuTextField[10]; // [10] : indexe de 0 a 9
  double cote_acc_[]= new double[10];
  // energies acc :
  BuTextField tf_energie_acc[]= new BuTextField[10];
  double energie_acc_[]= new double[10];
  // reactions acc : existences : JComboBox (Oui/Non)
  JComboBox jc_reaction_acc_[]= new JComboBox[10];
  BuTextField tf_reaction_acc[]= new BuTextField[10];
  double reaction_acc_[]= new double[10];
  // BuTextFields 05  : defense
  JComboBox jc_defense; // O/N
  BuTextField tf_hauteur_defense;
  double hauteur_defense_;
  // courbesED :
  JComboBox jc_nb_courbesED, jc_no_courbeED; // n� courbe ED
  int numero_courbeED_= 0, nombre_courbesED_= 0; // valeurs initiale
  JComboBox jc_iv, jc_nb_pointsED;
  int ied, nombre_pointsED_= 0;
  // int nombre_pointsED1_ = 0 , nombre_pointsED2_ = 0 , nombre_pointsED3_ = 0;
  // JComboBox jc_interpolation_courbesED;
  BuTextField tf_courbeED_cote_mini, tf_courbeED_cote_maxi;
  double courbeED_cote_mini_, courbeED_cote_maxi_;
  BuTextField tf_courbeED_def[]= new BuTextField[7]; // [7] : indexe de 0 a 6
  BuTextField tf_courbeED_eff[]= new BuTextField[7];
  double courbeED_def_[]= new double[7];
  double courbeED_eff_[]= new double[7];
  JComponent content_;
  BuCommonInterface appli_;
  // *******************************************
  // constructeur classe DimducFilleParametres *
  // *******************************************
  public DimducFilleParametres(final BuCommonInterface _appli) {
    super("", true, true, true, true);
    appli_= _appli;
    int n;
    // ***************************************
    //  Parametres panneau 01 : tete ouvrage *
    // ***************************************
    final JPanel pn01= new JPanel();
    final BuGridLayout lo01= new BuGridLayout();
    lo01.setColumns(4);
    lo01.setHgap(5);
    lo01.setVgap(5);
    pn01.setLayout(lo01);
    pn01.setBorder(new EmptyBorder(5, 5, 5, 5));
    n= 0;
    // declarations champs
    // tf_cote_tete
    tf_cote_tete= BuTextField.createDoubleField();
    tf_cote_tete.setText("0");
    cote_tete_= 0;
    tf_cote_tete.setColumns(10);
    // ecouteur de chgt addFocuslistener , declenche methode FocusListener
    tf_cote_tete.addFocusListener(this); // ecouteur
    tf_cote_tete.setName("PANEL01_COTE_TETE");
    // jc_fleche_tete
    jc_fleche_tete= new JComboBox();
    jc_fleche_tete.addItem("Oui");
    jc_fleche_tete.addItem("Non");
    jc_fleche_tete.setSelectedItem("Non");
    // ecouteur de chgt addItemlistener , declenche methode itemStateChanged(ItemEvent evt)
    jc_fleche_tete.addItemListener(this);
    jc_fleche_tete.setName("PANEL01_IS_FLECHE_TETE");
    // tf_fleche_tete
    tf_fleche_tete= BuTextField.createDoubleField();
    tf_fleche_tete.setText("0");
    fleche_tete_= 0.0;
    fleche_tete_anc_= 0.0;
    tf_fleche_tete.setColumns(10);
    // ecouteur de chgt addFocusListener, declenche methode focusLost(ActionEvent evt):
    tf_fleche_tete.addFocusListener(this); // ecouteur
    tf_fleche_tete.setEditable(false);
    tf_fleche_tete.setName("PANEL01_FLECHE_TETE");
    // setColumns : sets the column of the cell or label that is being selected or deselected.
    // construction panneau 01 : tete ouvrage  (4 col.) :
    pn01.add(new JLabel(" "), n++);
    pn01.add(new JLabel(" "), n++);
    pn01.add(new JLabel(" "), n++);
    pn01.add(new JLabel(" "), n++);
    pn01.add(new JLabel("Cote :", SwingConstants.RIGHT), n++);
    pn01.add(tf_cote_tete, n++);
    pn01.add(new JLabel("(en m)", SwingConstants.LEFT), n++);
    pn01.add(new JLabel(" "), n++);
    pn01.add(new JLabel(" "), n++);
    pn01.add(new JLabel(" "), n++);
    pn01.add(new JLabel(" "), n++);
    pn01.add(new JLabel(" "), n++);
    pn01.add(new JLabel(" Fl�che maximum ", SwingConstants.RIGHT), n++);
    pn01.add(jc_fleche_tete, n++);
    pn01.add(new JLabel(" (O/N)", SwingConstants.LEFT), n++);
    pn01.add(new JLabel(" ", SwingConstants.LEFT), n++);
    pn01.add(new JLabel(" admissible ", SwingConstants.RIGHT), n++);
    pn01.add(tf_fleche_tete, n++);
    pn01.add(new JLabel("( > 0, en m)", SwingConstants.LEFT), n++);
    pn01.add(new JLabel(" "), n++);
    // *******************************
    // Parametres panneau 02 : Tubes *
    // *******************************
    final JPanel pn02= new JPanel();
    final BuGridLayout lo02= new BuGridLayout();
    lo02.setColumns(4);
    lo02.setHfilled(true);
    lo02.setHgap(5);
    lo02.setVgap(5);
    pn02.setLayout(lo02);
    pn02.setBorder(new EmptyBorder(5, 5, 5, 5));
    n= 0;
    // declarations champs :
    // jc_nb_tubes : nombre de tubes
    jc_nb_tubes= new JComboBox();
    jc_nb_tubes.addItem("1");
    jc_nb_tubes.setSelectedItem("1");
    // jc_nb_tubes.addItem ("2"); V 1.2
    // ecouteur de changement :
    jc_nb_tubes.addItemListener(this); // ecouteur
    jc_nb_tubes.setEditable(false);
    jc_nb_tubes.setEnabled(false); // a  deverrouiller dans v1.2
    jc_nb_tubes.setName("PANEL02_NOMBRE_TUBES");
    // tf_module_young : module young acier :
    tf_module_young= BuTextField.createDoubleField();
    tf_module_young.setText("21");
    young_= 21;
    tf_module_young.setColumns(10);
    tf_module_young.addFocusListener(this); // ecouteur
    tf_module_young.setName("PANEL02_MODULE_YOUNG");
    // tf_entre_axes : distance entre axes si 2 tubes : v 2
    tf_entre_axes= BuTextField.createDoubleField();
    tf_entre_axes.setText("0");
    entre_axes_= 0;
    tf_entre_axes.setColumns(10);
    tf_entre_axes.addFocusListener(this); // ecouteur chgt
    tf_entre_axes.setEditable(false);
    tf_entre_axes.setName("PANEL02_ENTRE_AXES");
    // diametres :
    // mini :
    tf_diametre_mini= BuTextField.createDoubleField();
    tf_diametre_mini.setText("0");
    diametre_mini_= 0;
    tf_diametre_mini.setColumns(10);
    tf_diametre_mini.addFocusListener(this); // ecouteur
    tf_diametre_mini.setName("PANEL02_DIAMETRE_MINI");
    // maxi :
    tf_diametre_maxi= BuTextField.createDoubleField();
    tf_diametre_maxi.setText("0");
    diametre_maxi_= 0;
    tf_diametre_maxi.setColumns(10);
    tf_diametre_maxi.addFocusListener(this); // ecouteur chgt
    tf_diametre_maxi.setName("PANEL02_DIAMETRE_MAXI");
    // pas :
    tf_diametre_pas= BuTextField.createDoubleField();
    tf_diametre_pas.setText("0");
    diametre_pas_= 0;
    tf_diametre_pas.setColumns(10);
    tf_diametre_pas.addFocusListener(this); // ecouteur chgt
    tf_diametre_pas.setName("PANEL02_PAS_DIAMETRE");
    // epaisseurs :
    // mini :
    tf_epaisseur_mini= BuTextField.createDoubleField();
    tf_epaisseur_mini.setText("0");
    epaisseur_mini_= 0;
    tf_epaisseur_mini.setColumns(10);
    tf_epaisseur_mini.addFocusListener(this); // ecouteur chgt
    tf_epaisseur_mini.setName("PANEL02_EPAISSEUR_MINI");
    // maxi :
    tf_epaisseur_maxi= BuTextField.createDoubleField();
    tf_epaisseur_maxi.setText("0");
    epaisseur_maxi_= 0;
    tf_epaisseur_maxi.setColumns(10);
    tf_epaisseur_maxi.addFocusListener(this); // ecouteur chgt
    tf_epaisseur_maxi.setName("PANEL02_EPAISSEUR_MAXI");
    // pas :
    tf_epaisseur_pas= BuTextField.createDoubleField();
    tf_epaisseur_pas.setText("0");
    epaisseur_pas_= 0;
    tf_epaisseur_pas.setColumns(10);
    tf_epaisseur_pas.addFocusListener(this); // ecouteur chgt
    tf_epaisseur_pas.setName("PANEL02_PAS_EPAISSEUR");
    // corrodee :
    tf_epaisseur_cor= BuTextField.createDoubleField();
    tf_epaisseur_cor.setText("0");
    epaisseur_cor_= 0;
    tf_epaisseur_cor.setColumns(10);
    tf_epaisseur_cor.addFocusListener(this); // ecouteur chgt
    tf_epaisseur_cor.setName("PANEL02_EPAISSEUR_CORRODEE");
    // contraintes :
    // jc_nb_sigma : nombre contraintes :
    jc_nb_sigma= new JComboBox();
    jc_nb_sigma.addItem("1");
    jc_nb_sigma.setSelectedItem("1");
    jc_nb_sigma.addItem("2");
    jc_nb_sigma.addItem("3");
    jc_nb_sigma.addItem("4");
    jc_nb_sigma.addItem("5");
    jc_nb_sigma.addItem("6");
    jc_nb_sigma.addItem("7");
    jc_nb_sigma.addItem("8");
    jc_nb_sigma.setEditable(false);
    jc_nb_sigma.addItemListener(this); // ecouteur chgt
    jc_nb_sigma.setName("PANEL02_NOMBRE_CONTRAINTES");
    // tf_contrainte[] : boucle for :
    for (isigma= 0; isigma < 8; isigma++) {
      tf_contrainte[isigma]= BuTextField.createIntegerField();
      tf_contrainte[isigma].setText("0");
      contrainte_[isigma]= 0;
      tf_contrainte[isigma].setColumns(10);
      tf_contrainte[isigma].setEditable(false);
      tf_contrainte[isigma].addFocusListener(this); // ecouteur chgt
      tf_contrainte[isigma].setName("PANEL02_CONTRAINTE_" + (isigma + 1));
    } // fin boucle for
    tf_contrainte[0].setEditable(true); // le seul editable lors de la creation
    // construction panneau 02 : tubes  : (4 col.)
    pn02.add(new JLabel(" "), n++);
    pn02.add(new JLabel(" "), n++);
    pn02.add(new JLabel(" "), n++);
    pn02.add(new JLabel(" "), n++);
    pn02.add(new JLabel("Nombre tubes : ", SwingConstants.RIGHT), n++);
    pn02.add(jc_nb_tubes, n++);
    pn02.add(new JLabel("(1)", SwingConstants.LEFT), n++);
    pn02.add(new JLabel(" "), n++);
    /*/  v1.2
    pn02.add(new JLabel("D.entre axes : ",JLabel.RIGHT)              ,n++);
    pn02.add(tf_entre_axes                                           ,n++);
    pn02.add(new JLabel("(si 2 tubes : > 0, en m)",JLabel.LEFT)      ,n++);
    pn02.add(new JLabel (" ")                                        ,n++);
    // */
    pn02.add(new JLabel("Module d'Young : ", SwingConstants.RIGHT), n++);
    pn02.add(tf_module_young, n++);
    pn02.add(new JLabel("( > 0, en M�gat/m2)", SwingConstants.LEFT), n++);
    pn02.add(new JLabel(" "), n++);
    pn02.add(new JLabel(" "), n++);
    pn02.add(new JLabel(" "), n++);
    pn02.add(new JLabel(" "), n++);
    pn02.add(new JLabel(" "), n++);
    // en-tete epaisseurs et diametres :
    pn02.add(new JLabel(" "), n++);
    pn02.add(new JLabel(" Diam�tres ", SwingConstants.CENTER), n++);
    pn02.add(new JLabel(" Epaisseurs ", SwingConstants.CENTER), n++);
    pn02.add(new JLabel(" > 0, en mm ", SwingConstants.LEFT), n++);
    pn02.add(new JLabel("Mini : ", SwingConstants.RIGHT), n++);
    pn02.add(tf_diametre_mini, n++);
    pn02.add(tf_epaisseur_mini, n++);
    pn02.add(new JLabel(" ", SwingConstants.LEFT), n++);
    pn02.add(new JLabel("Maxi : ", SwingConstants.RIGHT), n++);
    pn02.add(tf_diametre_maxi, n++);
    pn02.add(tf_epaisseur_maxi, n++);
    pn02.add(new JLabel(" ", SwingConstants.LEFT), n++);
    pn02.add(new JLabel("Pas : ", SwingConstants.RIGHT), n++);
    pn02.add(tf_diametre_pas, n++);
    pn02.add(tf_epaisseur_pas, n++);
    pn02.add(new JLabel(" ", SwingConstants.LEFT), n++);
    pn02.add(new JLabel(" Corrod�e : ", SwingConstants.RIGHT), n++);
    pn02.add(new JLabel(" ", SwingConstants.LEFT), n++);
    pn02.add(tf_epaisseur_cor, n++);
    pn02.add(new JLabel(" >= 0", SwingConstants.LEFT), n++);
    pn02.add(new JLabel(" "), n++);
    pn02.add(new JLabel(" "), n++);
    pn02.add(new JLabel(" "), n++);
    pn02.add(new JLabel(" "), n++);
    // en-tete contraintes :
    pn02.add(new JLabel(" "), n++);
    pn02.add(new JLabel("Contraintes", SwingConstants.CENTER), n++);
    pn02.add(new JLabel("maximales", SwingConstants.CENTER), n++);
    pn02.add(new JLabel(" "), n++);
    pn02.add(new JLabel("Nombre :", SwingConstants.RIGHT), n++);
    pn02.add(jc_nb_sigma, n++);
    pn02.add(new JLabel(" (1 � 8)", SwingConstants.LEFT), n++);
    pn02.add(new JLabel(" ( > 0, en t/m2 )"), n++);
    // 1 a 4 :
    pn02.add(new JLabel(" N� 1 :", SwingConstants.CENTER), n++);
    pn02.add(new JLabel(" N� 2 :", SwingConstants.CENTER), n++);
    pn02.add(new JLabel(" N� 3 :", SwingConstants.CENTER), n++);
    pn02.add(new JLabel(" N� 4 :", SwingConstants.CENTER), n++);
    for (isigma= 0; isigma < 4; isigma++) {
      pn02.add(tf_contrainte[isigma], n++);
    }
    // 5 � 8 :
    pn02.add(new JLabel(" N� 5 :", SwingConstants.CENTER), n++);
    pn02.add(new JLabel(" N� 6 :", SwingConstants.CENTER), n++);
    pn02.add(new JLabel(" N� 7 :", SwingConstants.CENTER), n++);
    pn02.add(new JLabel(" N� 8 :", SwingConstants.CENTER), n++);
    for (isigma= 4; isigma < 8; isigma++) {
      pn02.add(tf_contrainte[isigma], n++);
    }
    // *****************************
    // Parametres panneau 03 : Sol *
    // *****************************
    final JPanel pn03= new JPanel();
    final BuGridLayout lo03= new BuGridLayout();
    lo03.setColumns(4);
    lo03.setHfilled(true);
    lo03.setHgap(5);
    lo03.setVgap(5);
    pn03.setLayout(lo03);
    pn03.setBorder(new EmptyBorder(5, 5, 5, 5));
    n= 0;
    // declarations champs :
    jc_nb_sols= new JComboBox();
    jc_nb_sols.addItem("1");
    jc_nb_sols.setSelectedItem("1");
    jc_nb_sols.addItem("2");
    // ecouteur de changement :
    jc_nb_sols.addItemListener(this);
    jc_nb_sols.setEditable(false);
    jc_nb_sols.setName("PANEL03_NOMBRE_SOLS");
    // sol 1 :
    // tf_cote_sup_1 :
    tf_cote_sup_1= BuTextField.createDoubleField();
    tf_cote_sup_1.setText("0");
    cote_sup_1_= 0;
    tf_cote_sup_1.setColumns(10);
    tf_cote_sup_1.addFocusListener(this); // ecouteur chgt
    tf_cote_sup_1.setName("PANEL03_COTE_SUP_1");
    // tf_butee_mini_1 :
    tf_butee_mini_1= BuTextField.createDoubleField();
    tf_butee_mini_1.setText("0");
    butee_mini_1_= 0;
    tf_butee_mini_1.setColumns(10);
    tf_butee_mini_1.addFocusListener(this); // ecouteur chgt
    tf_butee_mini_1.setName("PANEL03_BUTEE_MINI_1");
    // tf_butee_maxi_1            = new BuTextField("0",10);
    // tf_butee_maxi_1.setName("PANEL03_BUTEE_MAXI_1");
    // tf_cohesion_mini_1 :
    tf_cohesion_mini_1= BuTextField.createDoubleField();
    tf_cohesion_mini_1.setText("0");
    cohesion_mini_1_= 0;
    tf_cohesion_mini_1.setColumns(10);
    tf_cohesion_mini_1.addFocusListener(this); // ecouteur chgt
    tf_cohesion_mini_1.setName("PANEL03_COHESION_MINI_1");
    // tf_cohesion_maxi_1         = new BuTextField("0",10);
    // tf_cohesion_maxi_1.setName("PANEL03_COHESION_MAXI_1");
    // tf_poids_mini_1 :
    tf_poids_mini_1= BuTextField.createDoubleField();
    tf_poids_mini_1.setText("1.8");
    poids_mini_1_= 1.8;
    tf_poids_mini_1.setColumns(10);
    tf_poids_mini_1.addFocusListener(this); // ecouteur chgt
    tf_poids_mini_1.setName("PANEL03_POIDS_MINI_1");
    // tf_poids_maxi_1 :
    // tf_poids_maxi_1.setName("PANEL03_POIDS_MAXI_1");
    // sol 2 :
    // tf_cote_sup_2 :
    tf_cote_sup_2= BuTextField.createDoubleField();
    tf_cote_sup_2.setText("0");
    cote_sup_2_= 0;
    tf_cote_sup_2.setColumns(10);
    tf_cote_sup_2.setEditable(false);
    tf_cote_sup_2.addFocusListener(this); // ecouteur chgt
    tf_cote_sup_2.setName("PANEL03_COTE_SUP_2");
    // tf_butee_mini_2 :
    tf_butee_mini_2= BuTextField.createDoubleField();
    tf_butee_mini_2.setText("0");
    butee_mini_2_= 0;
    tf_butee_mini_2.setColumns(10);
    tf_butee_mini_2.setEditable(false);
    tf_butee_mini_2.addFocusListener(this); // ecouteur chgt
    tf_butee_mini_2.setName("PANEL03_BUTEE_MINI_2");
    // tf_butee_maxi_2            = new BuTextField("0",10);
    // tf_butee_maxi_2.setName("PANEL03_BUTEE_MAXI_2");
    // tf_cohesion_mini_2 :
    tf_cohesion_mini_2= BuTextField.createDoubleField();
    tf_cohesion_mini_2.setText("0");
    cohesion_mini_2_= 0;
    tf_cohesion_mini_2.setColumns(10);
    tf_cohesion_mini_2.setEditable(false);
    tf_cohesion_mini_2.addFocusListener(this); // ecouteur chgt
    tf_cohesion_mini_2.setName("PANEL03_COHESION_MINI_2");
    // tf_cohesion_maxi_2         = new BuTextField("0",10);
    // tf_cohesion_maxi_2.setName("PANEL03_COHESION_MAXI_2");
    // tf_poids_mini_2 :
    tf_poids_mini_2= BuTextField.createDoubleField();
    tf_poids_mini_2.setText("1.8");
    poids_mini_2_= 1.8;
    tf_poids_mini_2.setColumns(10);
    tf_poids_mini_2.setEditable(false);
    tf_poids_mini_2.addFocusListener(this); // ecouteur chgt
    tf_poids_mini_2.setName("PANEL03_POIDS_MINI_2");
    // tf_poids_maxi_2 :
    // tf_poids_maxi_2.setName("PANEL03_POIDS_MAXI_2");
    // construction panneau 03 sol :
    pn03.add(new JLabel(" "), n++);
    pn03.add(new JLabel(" "), n++);
    pn03.add(new JLabel(" "), n++);
    pn03.add(new JLabel(" "), n++);
    pn03.add(new JLabel("Nombre sols : ", SwingConstants.RIGHT), n++);
    pn03.add(jc_nb_sols, n++);
    pn03.add(new JLabel(" (1 ou 2)", SwingConstants.LEFT), n++);
    pn03.add(new JLabel(" "), n++);
    pn03.add(new JLabel(" "), n++);
    pn03.add(new JLabel(" "), n++);
    pn03.add(new JLabel(" "), n++);
    pn03.add(new JLabel(" "), n++);
    // en-tete sols :
    pn03.add(new JLabel(" "), n++);
    pn03.add(new JLabel(" SOL N� 1 ", SwingConstants.CENTER), n++);
    pn03.add(new JLabel(" SOL N� 2 ", SwingConstants.CENTER), n++);
    pn03.add(new JLabel(" "), n++);
    pn03.add(new JLabel("Cote sup. : ", SwingConstants.RIGHT), n++);
    pn03.add(tf_cote_sup_1, n++);
    pn03.add(tf_cote_sup_2, n++);
    pn03.add(new JLabel(" ( < cote t�te, en m)", SwingConstants.LEFT), n++);
    pn03.add(new JLabel("Coeff. but�e : ", SwingConstants.RIGHT), n++);
    pn03.add(tf_butee_mini_1, n++);
    pn03.add(tf_butee_mini_2, n++);
    pn03.add(new JLabel(" ( > 0, sans unit�)", SwingConstants.LEFT), n++);
    /*/
    pn03.add(new JLabel("But�e maxi : ",JLabel.RIGHT)                ,n++);
    pn03.add(tf_butee_maxi_1                                         ,n++);
    tf_butee_maxi_1.setEditable(false);
    pn03.add(tf_butee_maxi_2                                         ,n++);
    tf_butee_maxi_2.setEditable(false);
    pn03.add(new JLabel("(sans unit�)",JLabel.LEFT)                  ,n++);
    // */
    pn03.add(new JLabel("Coh�sion : ", SwingConstants.RIGHT), n++);
    pn03.add(tf_cohesion_mini_1, n++);
    pn03.add(tf_cohesion_mini_2, n++);
    pn03.add(new JLabel(" ( > 0, en t/m2)", SwingConstants.LEFT), n++);
    /*/
    pn03.add(new JLabel("Coh�sion maxi : ",JLabel.RIGHT)             ,n++);
    pn03.add(tf_cohesion_maxi_1                                      ,n++);
    tf_cohesion_maxi_1.setEditable(false);
    pn03.add(tf_cohesion_maxi_2                                      ,n++);
    tf_cohesion_maxi_2.setEditable(false);
    pn03.add(new JLabel("(en t/m2)",JLabel.LEFT)                     ,n++);
    // */
    pn03.add(new JLabel("Poids sp�c. : ", SwingConstants.RIGHT), n++);
    pn03.add(tf_poids_mini_1, n++);
    pn03.add(tf_poids_mini_2, n++);
    pn03.add(new JLabel(" ( > 0, en t/m3)", SwingConstants.LEFT), n++);
    /*/
    pn03.add(new JLabel("Poids maxi : ",JLabel.RIGHT)                ,n++);
    pn03.add(tf_poids_maxi_1                                         ,n++);
    tf_poids_maxi_1.setEditable(false);
    pn03.add(tf_poids_maxi_2                                         ,n++);
    tf_poids_maxi_2.setEditable(false);
    pn03.add(new JLabel("(en t/m3)",JLabel.LEFT)                     ,n++);
    // */
    // ************************************
    // Parametres panneau 04 : Accostages *
    // ************************************
    final JPanel pn04= new JPanel();
    final BuGridLayout lo04= new BuGridLayout();
    lo04.setColumns(5);
    lo04.setHgap(5);
    lo04.setVgap(5);
    pn04.setLayout(lo04);
    pn04.setBorder(new EmptyBorder(5, 5, 5, 5));
    n= 0;
    // declaration champs
    jc_nb_accostages= new JComboBox();
    jc_nb_accostages.addItem("1");
    jc_nb_accostages.setSelectedItem("1");
    jc_nb_accostages.addItem("2");
    jc_nb_accostages.addItem("3");
    jc_nb_accostages.addItem("4");
    jc_nb_accostages.addItem("5");
    jc_nb_accostages.addItem("6");
    jc_nb_accostages.addItem("7");
    jc_nb_accostages.addItem("8");
    jc_nb_accostages.addItem("9");
    jc_nb_accostages.addItem("10");
    // ecouteur de changement :
    jc_nb_accostages.addItemListener(this); // ecouteur chgt
    jc_nb_accostages.setEditable(false);
    jc_nb_accostages.setName("PANEL04_NOMBRE_ACCOSTAGES");
    // accostages :  1 a 10  en boucle for :
    for (iacc= 0; iacc < 10; iacc++) {
      tf_cote_acc[iacc]= BuTextField.createDoubleField();
      tf_cote_acc[iacc].setText("0");
      cote_acc_[iacc]= 0;
      tf_cote_acc[iacc].setColumns(10);
      tf_cote_acc[iacc].setEditable(false);
      tf_cote_acc[iacc].addFocusListener(this); // ecouteur chgt
      tf_cote_acc[iacc].setName("PANEL04_COTE_ACCOSTAGE_" + (iacc + 1));
      tf_energie_acc[iacc]= BuTextField.createDoubleField();
      tf_energie_acc[iacc].setText("0");
      energie_acc_[iacc]= 0;
      tf_energie_acc[iacc].setColumns(10);
      tf_energie_acc[iacc].setEditable(false);
      tf_energie_acc[iacc].addFocusListener(this); // ecouteur chgt
      tf_energie_acc[iacc].setName("PANEL04_ENERGIE_ACCOSTAGE_" + (iacc + 1));
      jc_reaction_acc_[iacc]= new JComboBox();
      jc_reaction_acc_[iacc].addItem("Oui");
      jc_reaction_acc_[iacc].addItem("Non");
      jc_reaction_acc_[iacc].setSelectedItem("Non");
      // ecouteur de chgt addItemlistener , declenche methode itemStateChanged(ItemEvent evt)
      jc_reaction_acc_[iacc].addItemListener(this); // ecouteur chgt
      jc_reaction_acc_[iacc].setEditable(false);
      jc_reaction_acc_[iacc].setName("PANEL04_IS_REACTION_ACC_" + (iacc + 1));
      tf_reaction_acc[iacc]= BuTextField.createDoubleField();
      tf_reaction_acc[iacc].setText("0");
      reaction_acc_[iacc]= 0;
      tf_reaction_acc[iacc].setColumns(10);
      tf_reaction_acc[iacc].addFocusListener(this); // ecouteur chgt
      tf_reaction_acc[iacc].setName("PANEL04_REACTION_ACCOSTAGE_" + (iacc + 1));
      tf_reaction_acc[iacc].setEditable(false);
    } // fin boucle for
    // acc 1 :
    tf_cote_acc[0].setText("10");
    cote_acc_[0]= 10;
    tf_cote_acc[0].setEditable(true);
    tf_energie_acc[0].setText("20");
    energie_acc_[0]= 20;
    tf_energie_acc[0].setEditable(true);
    // construction panneau 4 :  Accostages (5 col.)
    pn04.add(new JLabel(" "), n++);
    pn04.add(new JLabel(" "), n++);
    pn04.add(new JLabel(" "), n++);
    pn04.add(new JLabel(" "), n++);
    pn04.add(new JLabel(" "), n++);
    pn04.add(new JLabel("Nombre : ", SwingConstants.RIGHT), n++);
    pn04.add(jc_nb_accostages, n++);
    pn04.add(new JLabel(" (de 1 a 10)", SwingConstants.LEFT), n++);
    pn04.add(new JLabel(" "), n++);
    pn04.add(new JLabel(" "), n++);
    pn04.add(new JLabel(" "), n++);
    pn04.add(new JLabel(" "), n++);
    pn04.add(new JLabel(" "), n++);
    pn04.add(new JLabel(" "), n++);
    pn04.add(new JLabel(" "), n++);
    // en-tete tableau :
    pn04.add(new JLabel(" "), n++);
    pn04.add(new JLabel("Cotes", SwingConstants.CENTER), n++);
    pn04.add(new JLabel("Energies", SwingConstants.CENTER), n++);
    pn04.add(new JLabel("R�actions", SwingConstants.CENTER), n++);
    pn04.add(new JLabel("admiss.", SwingConstants.CENTER), n++);
    pn04.add(new JLabel(" "), n++);
    pn04.add(new JLabel("(en m) ", SwingConstants.CENTER), n++);
    pn04.add(new JLabel("( > 0, en tm)", SwingConstants.CENTER), n++);
    pn04.add(new JLabel(" ", SwingConstants.CENTER), n++);
    pn04.add(new JLabel("( > 0, en t)", SwingConstants.CENTER), n++);
    // valeurs accostages en boucle for :
    for (iacc= 0; iacc < 10; iacc++) {
      pn04.add(new JLabel(" Acc. n� " + (iacc + 1)), n++);
      pn04.add(tf_cote_acc[iacc], n++);
      pn04.add(tf_energie_acc[iacc], n++);
      pn04.add(jc_reaction_acc_[iacc], n++);
      pn04.add(tf_reaction_acc[iacc], n++);
    } // fin boucle for :
    // **************************************
    // Parametres Dimduc Onglet 5 : Defense *
    // **************************************
    final JPanel pn05= new JPanel();
    final BuGridLayout lo05= new BuGridLayout();
    lo05.setColumns(4);
    lo05.setHgap(5);
    lo05.setVgap(5);
    pn05.setLayout(lo05);
    pn05.setBorder(new EmptyBorder(5, 5, 5, 5));
    n= 0;
    // Courbe ED :
    // jc_defense
    jc_defense= new JComboBox();
    jc_defense.addItem("Oui");
    jc_defense.addItem("Non");
    jc_defense.setSelectedItem("Non");
    // ecouteur de chgt addItemlistener , declenche methode itemStateChanged(ItemEvent evt)
    jc_defense.addItemListener(this); // ecouteur chgt
    jc_defense.setName("PANEL01_IS_DEFENSE");
    // tf_hauteur_defense :
    tf_hauteur_defense= BuTextField.createDoubleField();
    tf_hauteur_defense.setText("0");
    hauteur_defense_= 0;
    tf_hauteur_defense.setColumns(10);
    tf_hauteur_defense.setEditable(false);
    tf_hauteur_defense.addFocusListener(this); // ecouteur chgt
    tf_hauteur_defense.setName("PANEL05_HAUTEUR_DEFENSE");
    // jc_nb_courbesED : nombre courbes ED
    jc_nb_courbesED= new JComboBox();
    jc_nb_courbesED.addItem("0");
    jc_nb_courbesED.setSelectedItem("0");
    jc_nb_courbesED.addItem("1");
    //jc_nb_courbesED.addItem ("2");
    //jc_nb_courbesED.addItem ("3");
    jc_nb_courbesED.setEditable(false);
    jc_nb_courbesED.setEnabled(false);
    jc_nb_courbesED.addItemListener(this); // ecouteur chgt  pour v ult.
    jc_nb_courbesED.setName("PANEL05_NOMBRE_COURBES_ED");
    //
    /* jc_interpolation_courbesED
    jc_interpolation_courbesED = new JComboBox();
    jc_interpolation_courbesED.addItem ("Oui");
    jc_interpolation_courbesED.addItem ("Non");
    jc_interpolation_courbesED.setSelectedItem("Non");

    jc_interpolation_courbesED.setEditable(false);
    jc_interpolation_courbesED.setEnabled(false);
    jc_interpolation_courbesED.addItemListener(this);
    jc_interpolation_courbesED.setName("PANEL05_INTERPOLATION_COURBES");


    // jc_no_courbeED :
    jc_no_courbeED = new JComboBox();
    jc_no_courbeED.addItem ("0");
    jc_no_courbeED.setSelectedItem("0");
    jc_no_courbeED.addItem ("1");
    jc_no_courbeED.addItem ("2");
    jc_no_courbeED.addItem ("3");

    jc_no_courbeED.setEditable(false);
    jc_no_courbeED.setEnabled(false);
    jc_no_courbeED.addItemListener(this);
    jc_no_courbeED.setName("PANEL05_NUMERO_COURBE_ED");
    // */
    // jc_nb_pointsEd : nombre de points courbe ED
    jc_nb_pointsED= new JComboBox();
    jc_nb_pointsED.addItem("0");
    jc_nb_pointsED.setSelectedItem("0");
    jc_nb_pointsED.addItem("1");
    jc_nb_pointsED.addItem("2");
    jc_nb_pointsED.addItem("3");
    jc_nb_pointsED.addItem("4");
    jc_nb_pointsED.addItem("5");
    jc_nb_pointsED.addItem("6");
    jc_nb_pointsED.addItem("7");
    jc_nb_pointsED.setEditable(false);
    jc_nb_pointsED.setEnabled(false);
    jc_nb_pointsED.addItemListener(this); // ecouteur chgt
    jc_nb_pointsED.setName("PANEL05_NOMBRE_POINTS_COURBE_ED");
    // jc_iv : intervalle de validite :
    jc_iv= new JComboBox();
    jc_iv.addItem("Oui");
    jc_iv.addItem("Non");
    jc_iv.setSelectedItem("Non");
    jc_iv.addItemListener(this); // ecouteur
    jc_iv.setEditable(false);
    jc_iv.setEnabled(false); // a  deverrouiller dans v1.2
    jc_iv.setName("PANEL05_IV");
    //  tf_courbeED_cote_mini, tf_courbeED_cote_maxi
    tf_courbeED_cote_mini= BuTextField.createDoubleField();
    tf_courbeED_cote_mini.setText("0");
    courbeED_cote_mini_= 0;
    tf_courbeED_cote_mini.setColumns(10);
    tf_courbeED_cote_mini.setEditable(false);
    tf_courbeED_cote_mini.addFocusListener(this); // ecouteur chgt
    tf_courbeED_cote_mini.setName("PANEL05_COURBEED_COTE_MINI");
    tf_courbeED_cote_maxi= BuTextField.createDoubleField();
    tf_courbeED_cote_maxi.setText("0");
    courbeED_cote_maxi_= 0;
    tf_courbeED_cote_maxi.setColumns(10);
    tf_courbeED_cote_maxi.setEditable(false);
    tf_courbeED_cote_maxi.addFocusListener(this); // ecouteur chgt
    tf_courbeED_cote_maxi.setName("PANEL05_COURBEED_COTE_MAXI");
    // boucle pour les 7 points pour saisie courbes ED, avec tableaux :
    for (ied= 0; ied < 7; ied++) {
      // tf_courbeED_def[
      tf_courbeED_def[ied]= BuTextField.createDoubleField();
      tf_courbeED_def[ied].setText("0");
      courbeED_def_[ied]= 0;
      tf_courbeED_def[ied].setColumns(10);
      tf_courbeED_def[ied].addFocusListener(this); // ecouteur chgt
      tf_courbeED_def[ied].setName("PANEL05_COURBEED_DEF_" + (ied + 1));
      tf_courbeED_def[ied].setEditable(false);
      // tf_courbeED_eff[
      tf_courbeED_eff[ied]= BuTextField.createDoubleField();
      tf_courbeED_eff[ied].setText("0");
      courbeED_eff_[ied]= 0;
      tf_courbeED_eff[ied].setColumns(10);
      tf_courbeED_eff[ied].addFocusListener(this); // ecouteur chgt
      tf_courbeED_eff[ied].setName("PANEL05_COURBEED_EFF_" + (ied + 1));
      tf_courbeED_eff[ied].setEditable(false);
    } // fin boucle for
    // construction panneau 05 : Defense (4 col.)
    // en-tete defense :
    pn05.add(new JLabel(" "), n++);
    pn05.add(new JLabel(" "), n++);
    pn05.add(new JLabel(" "), n++);
    pn05.add(new JLabel(" "), n++);
    pn05.add(new JLabel(" "), n++);
    pn05.add(new JLabel(" Pr�sence :", SwingConstants.RIGHT), n++);
    pn05.add(jc_defense, n++);
    pn05.add(new JLabel(" "), n++);
    pn05.add(new JLabel(" "), n++);
    pn05.add(new JLabel("Hauteur (mm) : ", SwingConstants.RIGHT), n++);
    pn05.add(tf_hauteur_defense, n++);
    pn05.add(new JLabel(" "), n++);
    pn05.add(new JLabel(" "), n++);
    pn05.add(new JLabel(" "), n++);
    pn05.add(new JLabel(" "), n++);
    pn05.add(new JLabel(" "), n++);
    pn05.add(new JLabel(" Courbes Effort ", SwingConstants.RIGHT), n++);
    pn05.add(new JLabel(" D�formation : ", SwingConstants.LEFT), n++);
    pn05.add(jc_nb_courbesED, n++);
    pn05.add(new JLabel("0 ou 1"), n++);
    /*pn05.add(new JLabel("Interpolation ",JLabel.RIGHT)               ,n++);
    pn05.add(new JLabel("courbes ED : ",JLabel.LEFT)                 ,n++);
    pn05.add(jc_interpolation_courbesED                              ,n++);
    pn05.add(new JLabel("(si 2 ou 3 courbes)",JLabel.LEFT)           ,n++);

    pn05.add(new JLabel(" Courbe Effort ",JLabel.RIGHT)              ,n++);
    pn05.add(new JLabel(" D�formation ",JLabel.LEFT)                 ,n++);
    // pn05.add(jc_no_courbeED                                          ,n++);
    pn05.add(new JLabel(" ")                                         ,n++);
    pn05.add(new JLabel(" ")                                         ,n++);
     // */
    pn05.add(new JLabel(" "), n++);
    pn05.add(new JLabel("Cotes validit� ", SwingConstants.RIGHT), n++);
    pn05.add(jc_iv, n++);
    pn05.add(new JLabel(" "), n++);
    pn05.add(new JLabel("Cote mini : ", SwingConstants.RIGHT), n++);
    pn05.add(tf_courbeED_cote_mini, n++);
    pn05.add(new JLabel("Cote maxi : ", SwingConstants.RIGHT), n++);
    pn05.add(tf_courbeED_cote_maxi, n++);
    pn05.add(new JLabel(" "), n++);
    pn05.add(new JLabel(" "), n++);
    pn05.add(new JLabel(" "), n++);
    pn05.add(new JLabel(" "), n++);
    // points de la courbe :
    pn05.add(new JLabel("Nombre points : ", SwingConstants.RIGHT), n++);
    pn05.add(jc_nb_pointsED, n++);
    pn05.add(new JLabel("(de 0 a 7)", SwingConstants.LEFT), n++);
    pn05.add(new JLabel(" "), n++);
    // tableau des points :
    // titre
    pn05.add(new JLabel(" "), n++);
    pn05.add(new JLabel("DEFORM. (en %)", SwingConstants.CENTER), n++);
    pn05.add(new JLabel("EFFORT  (en t)", SwingConstants.CENTER), n++);
    pn05.add(new JLabel(" "), n++);
    for (ied= 0; ied < 7; ied++) {
      pn05.add(new JLabel(" Point n� " + (ied + 1), SwingConstants.CENTER), n++);
      pn05.add(tf_courbeED_def[ied], n++);
      pn05.add(tf_courbeED_eff[ied], n++);
      pn05.add(new JLabel(" "), n++);
    } // fin boucle for
    // ********************
    // construction panel *
    // ********************
    final JTabbedPane tpMain= new JTabbedPane();
    // cd01 voit remplacement par DimducDonneesGenerales
    // tpMain.addTab(" Donn�es G�n�rales ", null, pnDonneesGenerales, "Donn�es G�n�rales");
    final JPanel cd01= new JPanel();
    cd01.setLayout(new BorderLayout());
    cd01.add("North", pn01);
    tpMain.addTab(" T�te ouvrage ", null, cd01, "Param�tres T�te ouvrage");
    // cd02
    final JPanel cd02= new JPanel();
    cd02.setLayout(new BorderLayout());
    cd02.add("North", pn02);
    tpMain.addTab(" Tubes ", null, cd02, "Param�tres Tubes");
    // cd03
    final JPanel cd03= new JPanel();
    cd03.setLayout(new BorderLayout());
    cd03.add("North", pn03);
    tpMain.addTab(" Sols ", null, cd03, "Param�tres Sols ");
    // cd04
    final JPanel cd04= new JPanel();
    cd04.setLayout(new BorderLayout());
    cd04.add("North", pn04);
    tpMain.addTab(" Accostages ", null, cd04, "Param�tres Accostages ");
    // cd05
    final JPanel cd05= new JPanel();
    cd05.setLayout(new BorderLayout());
    cd05.add("North", pn05);content_= (JComponent)getContentPane();
    content_.setBorder(new EmptyBorder(5, 5, 5, 5));
    content_.add("Center", tpMain);
    tpMain.addTab(" D�fense ", null, cd05, "Param�tres D�fense ");
    content_= (JComponent)getContentPane();
    content_.setBorder(new EmptyBorder(5, 5, 5, 5));
    content_.add("Center", tpMain);
    setTitle("Param�tres de calcul");
    setFrameIcon(BuResource.BU.getIcon("Parametres"));
    setLocation(40, 40);
    pack();
    //setSize(new Dimension(600,700));
  } // fin DimducFilleParametres
  // ********************
  // METHODES PUBLIQUES *
  // ********************
  // Suite � changement d'etat des composants JComboBox  :  jc_fleche_tete, jc_nb_tubes,
  // jc_nb_sigma,  jc_nb_sols, jc_nb_accostages
  // void effaceBuTextField *double***********************
  public void effaceJtfI(final BuTextField jtf_, int int_)  // *****************************************************
  {
    jtf_.setEditable(false);
    jtf_.setText("0");
    int_= 0;
  }
  // void effaceBuTextField *double***********************
  public void effaceJtfD(final BuTextField jtf_, double double_)  // *****************************************************
  {
    jtf_.setEditable(false);
    jtf_.setText("0");
    double_= 0.0;
  }
  // public void afficherFlecheTete ************
  public void afficherFlecheTete(final Object objet_)  // *******************************************
  {
    if (objet_.equals("Oui")) {
      tf_fleche_tete.setEditable(true);
      System.out.println("object = Oui, fleche_tete_ = " + fleche_tete_);
      fleche_tete_anc_= 0.0;
      tf_fleche_tete.setText("" + fleche_tete_);
    } else if (objet_.equals("Non")) {
      fleche_tete_anc_= fleche_tete_;
      effaceJtfD(tf_fleche_tete, fleche_tete_);
      System.out.println("object = Non, fleche_tete_ = " + fleche_tete_);
      System.out.println(
        "object = Non, fleche_tete_anc_ = " + fleche_tete_anc_);
    }
  } //  fin void afficherFlecheTete
  // public void afficherEntreAxe ************
  public void afficherEntreAxe(final int nb_tubes_)  // *****************************************
  {
    if (nb_tubes_ == 1) {
      effaceJtfD(tf_entre_axes, entre_axes_);
    } else if (nb_tubes_ == 2) {
      tf_entre_axes.setEditable(true);
    }
  } // fin void afficherEntreAxe
  // public void afficherContraintes *********************
  public void afficherContraintes(final int nb_contraintes_)  // *****************************************************
  { // adaptation en boucle for :
    for (isigma= 0; isigma < 8; isigma++) {
      if (isigma >= nombre_contraintes_) {
        effaceJtfI(tf_contrainte[isigma], contrainte_[isigma]);
      }
      // fin if (isigma <= nombre_contraintes)
      else if (isigma < nombre_contraintes_) {
        tf_contrainte[isigma].setEditable(true);
      } // fin else if
    } // fin boucle for
  } // fin public void afficherContraintes
  // public void afficherSols ****************
  public void afficherSols(final int nb_sols)  // *********************************************
  {
    switch (nb_sols) {
      case 1 :
        // sol 2 neutralise et efface :
        effaceJtfD(tf_cote_sup_2, cote_sup_2_);
        effaceJtfD(tf_butee_mini_2, butee_mini_2_);
        effaceJtfD(tf_cohesion_mini_2, cohesion_mini_2_);
        effaceJtfD(tf_poids_mini_2, poids_mini_2_);
        break;
      case 2 :
        // sols 1 et 2 editables :
        tf_cote_sup_2.setEditable(true);
        tf_butee_mini_2.setEditable(true);
        //tf_butee_maxi_2.setEditable (false);
        tf_cohesion_mini_2.setEditable(true);
        //tf_cohesion_maxi_2.setEditable (false);
        tf_poids_mini_2.setText("1.9");
        poids_mini_2_= 1.9;
        tf_poids_mini_2.setEditable(true);
        // tf_poids_maxi_2.setEditable (false);
        break;
    } // fin switch nb_sols
  } // fin void afficherSols
  // public void afficherAccostages **********
  public void afficherAccostages(final int nb_acc_)  // *****************************************
  { // adaptation en boucle for :
    for (iacc= 0; iacc < 10; iacc++) {
      if (iacc >= nb_acc_) {
        effaceJtfD(tf_cote_acc[iacc], cote_acc_[iacc]);
        effaceJtfD(tf_energie_acc[iacc], energie_acc_[iacc]);
        jc_reaction_acc_[iacc].setSelectedItem("Non");
        jc_reaction_acc_[iacc].setEnabled(false);
        effaceJtfD(tf_reaction_acc[iacc], reaction_acc_[iacc]);
      } // fin if (iacc <= nombre_accostages_)
      else if (iacc < nombre_accostages_) {
        tf_cote_acc[iacc].setEditable(true);
        tf_energie_acc[iacc].setEditable(true);
        jc_reaction_acc_[iacc].setEnabled(true);
      } // fin else if
    } // fin boucle for
  } // fin void afficherAccostages
  // public void afficherIV *************
  public void afficherIV(final Object objet_)  // ************************************
  {
    if (objet_.equals("Oui")) {
      tf_courbeED_cote_mini.setEditable(true);
      tf_courbeED_cote_maxi.setEditable(true);
    } else if (objet_.equals("Non")) {
      effaceJtfD(tf_courbeED_cote_mini, courbeED_cote_mini_);
      effaceJtfD(tf_courbeED_cote_maxi, courbeED_cote_maxi_);
    } // fin if (objet_.equals("Non")
  } //  fin void afficherIV
  // public void afficherDefense ** ************
  public void afficherDefense(final Object objet_)  // *******************************************
  {
    if (objet_.equals("Oui")) {
      tf_hauteur_defense.setEditable(true);
      jc_iv.setEnabled(true);
      jc_iv.setSelectedItem("Non");
      // jc_nb_courbesED.setEnabled(true);
      jc_nb_courbesED.setSelectedItem("1");
      nombre_courbesED_= 1; // a modifier v ult
      // jc_no_courbeED.setEnabled(true);
      jc_nb_pointsED.setEnabled(true);
      jc_nb_pointsED.setSelectedItem("2");
    } else if (objet_.equals("Non")) {
      effaceJtfD(tf_hauteur_defense, hauteur_defense_);
      jc_iv.setSelectedItem("Non");
      afficherIV("Non");
      jc_nb_courbesED.setSelectedItem("0");
      nombre_courbesED_= 0;
      jc_nb_courbesED.setEnabled(false);
      /*/    v ult
      jc_no_courbeED.setSelectedItem("0");
      numero_courbeED_ = 0;
      jc_no_courbeED.setEnabled(false);
      // */
      jc_nb_pointsED.setSelectedItem("0");
      nombre_pointsED_= 0;
      jc_nb_pointsED.setEnabled(false);
      for (ied= 0; ied < 7; ied++) {
        effaceJtfD(tf_courbeED_def[ied], courbeED_def_[ied]);
        effaceJtfD(tf_courbeED_eff[ied], courbeED_eff_[ied]);
      } // fin boucle for ied
    } // fin if (objet_.equals("Non")
  } //  fin void afficherDefense
  // public void afficherCourbeED ****************
  public void afficherCourbeED(final int nb_pointsED)  // *********************************************
  {
    // adaptation en boucle for :
    for (ied= 0; ied < 7; ied++) {
      if (ied >= nb_pointsED) {
        effaceJtfD(tf_courbeED_def[ied], courbeED_def_[ied]);
        effaceJtfD(tf_courbeED_eff[ied], courbeED_eff_[ied]);
      } // fin if (ied <= nb_pointsEd)
      else if (ied < nb_pointsED) {
        tf_courbeED_def[ied].setEditable(true);
        tf_courbeED_eff[ied].setEditable(true);
      } // fin else if
    } // fin boucle for
  } // fin public void afficherCourbeED
  // ******************************************************************************************
  // getStateChanged : gestion des changements, suite a ecouteurs de changement ItemListener *
  // ******************************************************************************************
  public void getStateChanged(final ItemEvent evt) {
    //Object source = evt.getSource ();
    // non gere pour l'instant
  } // fin void
  // ******************************************************************************************
  // itemStateChanged : gestion des changements, suite a ecouteurs de changement ItemListener *
  // ******************************************************************************************
  public void itemStateChanged(final ItemEvent evt) {
    final Object source= evt.getSource();
    // ****************************************
    // source changement : fleche en tete O/N *
    // ****************************************
    if (source == jc_fleche_tete) {
      afficherFlecheTete(evt.getItem());
    }
    // *************************************
    // source changement : nombre de tubes *
    // *************************************
    if (source == jc_nb_tubes) {
      nombre_tubes_= jc_nb_tubes.getSelectedIndex() + 1;
      afficherEntreAxe(nombre_tubes_);
    } // fin  if (source == jc_nb_tubes)
    // *******************************************
    // source changement : nombre de contraintes *
    // *******************************************
    if (source == jc_nb_sigma) {
      nombre_contraintes_= jc_nb_sigma.getSelectedIndex() + 1;
      afficherContraintes(nombre_contraintes_);
    } // fin if (source == jc_nb_sigma)
    // ************************************
    // source changement : nombre de sols *
    // ************************************
    if (source == jc_nb_sols) {
      nombre_sols_= jc_nb_sols.getSelectedIndex() + 1;
      afficherSols(nombre_sols_);
    } // fin  if (source == jc_nb_sols)
    // *****************************************
    // source changement : nombre d'accostages *
    // *****************************************
    if (source == jc_nb_accostages) {
      nombre_accostages_= jc_nb_accostages.getSelectedIndex() + 1;
      afficherAccostages(nombre_accostages_);
    }
    // *********************************************
    // source changement : reactions accostage O/N *
    // *********************************************
    // jc_reaction_acc_ en boucle for :
    for (iacc= 0; iacc < 10; iacc++) {
      if (source == jc_reaction_acc_[iacc]) {
        final Object newOption= evt.getItem();
        if (newOption.equals("Oui")) {
          tf_reaction_acc[iacc].setEditable(true);
        } else if (newOption.equals("Non")) {
          effaceJtfD(tf_reaction_acc[iacc], reaction_acc_[iacc]);
        }
      } // fin if (source == jc_reaction_acc_[iacc])
    } // fin boucle for
    // *******************************
    // source changement :jc_defense *
    // *******************************
    if (source == jc_defense) {
      afficherDefense(evt.getItem());
    }
    // **************************
    // source changement :jc_iv *
    // **************************
    if (source == jc_iv) {
      afficherIV(evt.getItem());
    }
    // *****************************************
    // source changement : nombre courbes ED    *
    // *****************************************
    // v 1.2
    /*
    if (source == jc_nb_courbesED)
    {
     nombre_courbesED_ =  jc_nb_courbesED.getSelectedIndex();
     afficherCourbesED (nombre_courbesED_);

    }         // fin source == jc_nb_courbesED


    // *****************************************
    // source changement : numero courbe ED    *
    // *****************************************

    if (source == jc_no_courbeED)
    {
     numero_courbeED_ =  jc_no_courbeED.getSelectedIndex();
     // afficherCourbesED (numero_courbeED_);

    }         // fin source == jc_no_courbesED
    // */
    // *****************************************
    // source changement : nombre points ED    *
    // *****************************************
    if (source == jc_nb_pointsED) {
      nombre_pointsED_= jc_nb_pointsED.getSelectedIndex();
      afficherCourbeED(nombre_pointsED_);
    } // fin source == jc_nb_pointsED
  } // fin  void itemStateChanged(ItemEvent evt)
  // ******************
  // CONTROLES SAISIE *
  // ******************
  // *********************************************************************************************
  // FOCUSGAINED&LOST : Gestion des changements, suite a ecouteur perte de focus (ActionFocusListener *
  // *********************************************************************************************
  // **********************************
  // FOCUSGAINED : arrivee : non gere *
  // **********************************
  public void focusGained(final FocusEvent evt) {
    //  Object source = evt.getSource ();
  } // fin void focusGained : non gere
  // ********************
  // focusLost : depart *
  // ********************
  public void focusLost(final FocusEvent evt) {
    final Object source= evt.getSource();
    // tests source de changement, conversions et controles :
    // donnees generales :
    // source changement : cote tete :
    if (source == tf_cote_tete) {
      if (cote_sup_1_ != 0) {
        it_=
          new DimducTextField(
            appli_,
            "de la cote sup. du sol 1",
            "de la cote en t�te",
            tf_cote_sup_1,
            tf_cote_tete);
        it_.miniInfMaxiReels();
        if (it_.miniInfMaxi_) {
          cote_tete_= it_.doubleTextField2();
        }
      }
    } // fin if (source == tf_cote_tete)
    // source changement : fleche en tete :
    if (source == tf_fleche_tete) {
      if (jc_fleche_tete.getSelectedItem().equals("Oui")) {
        it_=
          new DimducTextField(appli_, "de la fl�che en t�te", tf_fleche_tete);
        it_.positifReel();
        if (it_.positif_) {
          fleche_tete_anc_= fleche_tete_;
          fleche_tete_= it_.doubleTextField1();
          System.out.println(
            " chgt fleche_tete_ : anc = "
              + fleche_tete_anc_
              + " new = "
              + fleche_tete_);
        } // fin if (it_.positif_)
      } // fin if (jc_fleche_tete.getSelectedItem().equals("Oui"))
    } // fin if (source == tf_fleche_tete)
    // source changement : module d'young :
    if (source == tf_module_young) {
      it_= new DimducTextField(appli_, "du module d'Young", tf_module_young);
      it_.positifReel();
      if (it_.positif_) {
        young_= it_.doubleTextField1();
      } else {
        young_= 0.0;
      }
      System.out.println("young = " + young_);
    }
    // source changement : entre_axes : dans v 2
    if (source == tf_entre_axes) {
      if (nombre_tubes_ == 2) {
        it_=
          new DimducTextField(
            appli_,
            "de la distance entre axes",
            tf_entre_axes);
        it_.positifReel();
        if (it_.positif_) {
          entre_axes_= it_.doubleTextField1();
        }
      }
    }
    // source changement : diametres :
    // diametre mini :
    if (source == tf_diametre_mini) {
      it_= new DimducTextField(appli_, "du diam�tre mini", tf_diametre_mini);
      it_.positifReel();
      if (it_.positif_)        // comparaison avec diametre maxi si deja saisi :
        {
        if (diametre_maxi_ > 0) {
          it_=
            new DimducTextField(
              appli_,
              "du diam�tre mini",
              "du diam�tre maxi",
              tf_diametre_mini,
              tf_diametre_maxi);
          it_.miniInfMaxiReels();
          if (it_.miniInfMaxi_) {
            diametre_mini_= it_.doubleTextField1();
          }
        } // fin if (diametre_maxi_ > 0)
      } // fin if it_.positif
    } // fin if (source == tf_diametre_mini)
    // diametre maxi :
    if (source == tf_diametre_maxi) {
      it_=
        new DimducTextField(
          appli_,
          "du diam�tre mini",
          "du diam�tre maxi",
          tf_diametre_mini,
          tf_diametre_maxi);
      it_.miniInfMaxiReels();
      if (it_.miniInfMaxi_) {
        diametre_maxi_= it_.doubleTextField2();
      }
    }
    // source changement : epaisseurs :
    // epaisseur mini :
    if (source == tf_epaisseur_mini) {
      it_=
        new DimducTextField(appli_, "de l'�paisseur mini", tf_epaisseur_mini);
      it_.positifReel();
      if (it_.positif_)        // comparaison avec epaisseur maxi si deja saisie :
        {
        if (epaisseur_maxi_ > 0) {
          it_=
            new DimducTextField(
              appli_,
              "de l'�paisseur mini",
              "de l'�paisseur maxi",
              tf_epaisseur_mini,
              tf_epaisseur_maxi);
          it_.miniInfMaxiReels();
          if (it_.miniInfMaxi_) {
            epaisseur_mini_= it_.doubleTextField1();
          }
        } // fin if (epaisseur_maxi_ > 0)
      } // fin if it_.positif
    } // fin if (source == tf_epaisseur_mini)
    // epaisseur maxi
    if (source == tf_epaisseur_maxi) {
      it_=
        new DimducTextField(
          appli_,
          "de l'�paisseur mini",
          "de l'�paisseur maxi",
          tf_epaisseur_mini,
          tf_epaisseur_maxi);
      it_.miniInfMaxiReels();
      if (it_.miniInfMaxi_) {
        epaisseur_maxi_= it_.doubleTextField2();
      }
    } // fin (source == tf_epaisseur_maxi)
    // source changement : pas diametre :
    if (source == tf_diametre_pas) {
      it_= new DimducTextField(appli_, "du pas de diam�tre", tf_diametre_pas);
      it_.positifReel();
      if (it_.positif_) {
        diametre_pas_= it_.doubleTextField1();
      }
    }
    // source changement : pas epaisseur :
    if (source == tf_epaisseur_pas) {
      it_= new DimducTextField(appli_, "du pas d'�paisseur", tf_epaisseur_pas);
      it_.positifReel();
      if (it_.positif_) {
        epaisseur_pas_= it_.doubleTextField1();
      }
    }
    // source changement : epaisseur corrodee :
    if (source == tf_epaisseur_cor) {
      it_=
        new DimducTextField(
          appli_,
          "de l'�paisseur corrod�e",
          tf_epaisseur_cor);
      it_.positifOuNulReel();
      if (it_.positifOuNul_) {
        epaisseur_cor_= it_.doubleTextField1();
      }
    }
    // source changement : contraintes :
    for (isigma= 0; isigma < 8; isigma++) {
      if (source == tf_contrainte[isigma]) {
        if (jc_nb_sigma.getSelectedIndex() >= isigma) {
          it_=
            new DimducTextField(
              appli_,
              "de la contrainte" + (isigma + 1),
              tf_contrainte[isigma]);
          it_.positifEntier();
          if (it_.positif_) {
            contrainte_[isigma]= it_.intTextField();
          } // fin if it_ positif_
        } // fin if (jc_nb_sigma.getSelectedIndex() >= isigma)
      } // fin if (source == tf_contrainte[isigma])
    } // fin boucle for isigma
    // source changement : sols :
    // sol 1 :
    // cote sup 1 :
    if (source == tf_cote_sup_1) {
      if (cote_tete_ != 0.0) {
        it_=
          new DimducTextField(
            appli_,
            "de la cote sup du sol 1",
            "de la cote en t�te",
            tf_cote_sup_1,
            tf_cote_tete);
        it_.miniInfMaxiReels();
        if (it_.miniInfMaxi_) {
          if (cote_sup_2_ != 0.0) {
            it_=
              new DimducTextField(
                appli_,
                "de la cote sup du sol 2",
                "de la cote sup du sol 1",
                tf_cote_sup_2,
                tf_cote_sup_1);
            it_.miniInfMaxiReels();
            if (it_.miniInfMaxi_) {
              cote_sup_1_= it_.doubleTextField2();
            }
          } // fin (if cote_sup_2 != 0)
        } // fin  if (it_.miniInfMaxi_)
      } // fin if (cote_tete_ != 0.0)
    } // fin source == tf_cote_sup_1
    // butee ou cohesion > 0 :
    // butee 1 mini
    if (source == tf_butee_mini_1) {
      it_=
        new DimducTextField(
          appli_,
          "du coeff. de but�e 1 mini",
          tf_butee_mini_1);
      it_.positifOuNulReel();
      if (it_.positifOuNul_) {
        it_=
          new DimducTextField(
            appli_,
            "du coeff. de but�e 1 mini",
            "de la coh�sion 1 mini",
            tf_butee_mini_1,
            tf_cohesion_mini_1);
        it_.positif1Des2Reels();
        if (it_.positif1Des2_) {
          butee_mini_1_= it_.doubleTextField1();
        }
      } // fin if it_positifOuNu
    } // (fin if source == tf_butee_mini_1)
    // cohesion 1 mini :
    if (source == tf_cohesion_mini_1) {
      it_=
        new DimducTextField(
          appli_,
          "de la coh�sion 1 mini",
          tf_cohesion_mini_1);
      it_.positifOuNulReel();
      if (it_.positifOuNul_) {
        it_=
          new DimducTextField(
            appli_,
            "du coeff. de but�e 1 mini",
            "de la coh�sion 1 mini",
            tf_butee_mini_1,
            tf_cohesion_mini_1);
        it_.positif1Des2Reels();
        if (it_.positif1Des2_) {
          cohesion_mini_1_= it_.doubleTextField2();
        }
      } // fin if (it_.positifOuNul_)
    } // fin if (source == tf_cohesion_mini_1)
    // poids mini 1 :
    if (source == tf_poids_mini_1) {
      it_=
        new DimducTextField(
          appli_,
          "du poids volumique 1 mini",
          tf_poids_mini_1);
      it_.positifReel();
      if (it_.positif_) {
        poids_mini_1_= it_.doubleTextField1();
      }
    }
    // sol 2 :
    // cote sup 2 :
    if (source == tf_cote_sup_2) {
      if (jc_nb_sols.getSelectedItem().equals("2")) {
        it_=
          new DimducTextField(
            appli_,
            "de la cote sup du sol 2",
            "de la cote sup du sol 1",
            tf_cote_sup_2,
            tf_cote_sup_1);
        it_.miniInfMaxiReels();
        if (it_.miniInfMaxi_) {
          cote_sup_2_= it_.doubleTextField1();
        }
      }
    }
    // butee mini 2 :
    if (source == tf_butee_mini_2) {
      if (jc_nb_sols.getSelectedItem().equals("2")) {
        it_=
          new DimducTextField(
            appli_,
            "du coeff. de but�e 2 mini",
            tf_butee_mini_2);
        it_.positifOuNulReel();
        if (it_.positifOuNul_) {
          it_=
            new DimducTextField(
              appli_,
              "du coeff. de but�e 2 mini",
              "de la coh�sion 2 mini",
              tf_butee_mini_2,
              tf_cohesion_mini_2);
          it_.positif1Des2Reels();
          if (it_.positif1Des2_) {
            butee_mini_2_= it_.doubleTextField1();
          }
        } // fin if it_ positifouNul
      } // fin if jc_nb_sols
    } // fin if source == tf_butee_mini_2
    // cohesion mini 2 :
    if (source == tf_cohesion_mini_2) {
      if (jc_nb_sols.getSelectedItem().equals("2")) {
        it_=
          new DimducTextField(
            appli_,
            "de la coh�sion 2 mini ",
            tf_cohesion_mini_2);
        it_.positifOuNulReel();
        if (it_.positifOuNul_) {
          it_=
            new DimducTextField(
              appli_,
              "du coeff. de but�e 2 mini",
              "de la coh�sion 2 mini",
              tf_butee_mini_2,
              tf_cohesion_mini_2);
          it_.positif1Des2Reels();
          if (it_.positif1Des2_) {
            cohesion_mini_2_= it_.doubleTextField2();
          }
        } // fin if it_ positifOunul
      } // fin if  {if (jc_nb_sols.getSelectedItem().equals("2"))
    } //  fin if (source == tf_cohesion_mini_2)
    // poids mini 2 :
    if (source == tf_poids_mini_2) {
      if (jc_nb_sols.getSelectedItem().equals("2")) {
        it_=
          new DimducTextField(
            appli_,
            "du poids volumique 2 mini",
            tf_poids_mini_2);
        it_.positifReel();
        if (it_.positif_) {
          poids_mini_2_= it_.doubleTextField1();
        }
      }
    }
    // accostages : en boucle for :
    for (iacc= 0; iacc < nombre_accostages_; iacc++)      // cote acc : double verification :
      {
      if (source == tf_cote_acc[iacc]) {
        it_=
          new DimducTextField(
            appli_,
            "de la cote sup du sol 1",
            "de la cote de l'accostage " + (iacc + 1),
            tf_cote_sup_1,
            tf_cote_acc[iacc]);
        it_.miniInfMaxiReels();
        if (it_.miniInfMaxi_) {
          it_=
            new DimducTextField(
              appli_,
              "de la cote de l'accostage " + (iacc + 1),
              "de la cote en t�te",
              tf_cote_acc[iacc],
              tf_cote_tete);
          it_.miniInfMaxiReels();
          if (it_.miniInfMaxi_) {
            cote_acc_[iacc]= it_.doubleTextField1();
          }
        } // fin if (it_.miniInfMaxi_ 1)
      }
      // energie d'accostage :
      if (source == tf_energie_acc[iacc]) {
        it_=
          new DimducTextField(
            appli_,
            "de l'�nergie de l'accostage " + (iacc + 1),
            tf_energie_acc[iacc]);
        it_.positifReel();
        if (it_.positif_) {
          energie_acc_[iacc]= it_.doubleTextField1();
        }
      }
      // reaction admissible :
      if (source == tf_reaction_acc[iacc]) {
        if (jc_reaction_acc_[iacc].getSelectedItem().equals("Oui")) {
          it_=
            new DimducTextField(
              appli_,
              "de la r�action de l'accostage" + (iacc + 1),
              tf_reaction_acc[iacc]);
          it_.positifReel();
          if (it_.positif_) {
            reaction_acc_[iacc]= it_.doubleTextField1();
          }
        }
      } // fin if (source == tf_reaction_acc[iacc])
    } // fin boucle for
    // *******
    // Defense
    // *******
    // hauteur
    if (source == tf_hauteur_defense) {
      if (jc_defense.getSelectedItem().equals("Oui")) {
        it_=
          new DimducTextField(
            appli_,
            "de la hauteur de la d�fense",
            tf_hauteur_defense);
        it_.positifReel();
        if (it_.positif_) {
          hauteur_defense_= it_.doubleTextField1();
        }
      } // fin if (jc_defense.getSelectedItem().equals("Oui"))
    } // fin if (source == tf_hauteur_defense)
    // cote maxi
    if (source == tf_courbeED_cote_maxi) {
      if (jc_iv.getSelectedItem().equals("Oui")) {
        it_=
          new DimducTextField(
            appli_,
            "de la cote mini de la courbe ED",
            "de la cote maxi de la courbe ED",
            tf_courbeED_cote_mini,
            tf_courbeED_cote_maxi);
        it_.miniInfMaxiReels();
        if (it_.miniInfMaxi_) {
          it_=
            new DimducTextField(
              appli_,
              "de la cote maxi de la courbe ED",
              "de la cote en t�te",
              tf_courbeED_cote_maxi,
              tf_cote_tete);
          it_.miniInfMaxiReels();
          if (it_.miniInfMaxi_) {
            courbeED_cote_maxi_= it_.doubleTextField1();
          }
        } // fin if it_.miniInfMaxi1
      } // (fin (jc_iv.getSelectedItem().equals("Oui"))
    } // (fin source == tf_courbeED_cote_maxi)
    // cote mini
    if (source == tf_courbeED_cote_mini) {
      if (jc_iv.getSelectedItem().equals("Oui")) {
        if (courbeED_cote_maxi_ != 0) {
          it_=
            new DimducTextField(
              appli_,
              "de la cote mini de la courbe ED",
              "de la cote maxi de la courbe ED",
              tf_courbeED_cote_mini,
              tf_courbeED_cote_maxi);
          it_.miniInfMaxiReels();
          if (it_.miniInfMaxi_) {
            courbeED_cote_mini_= it_.doubleTextField1();
          }
        } // fin if(courbeED_cote_maxi_ != 0)
        it_= null;
      } // (fin (jc_iv.getSelectedItem().equals("Oui"))
    } // (fin source == tf_courbeED_cote_mini)
    // points ED :
    // pt 1 a 7 :
    for (ied= 0; ied < nombre_pointsED_; ied++)      // deformations : positives et croissantes :
      {
      if (source == tf_courbeED_def[ied]) {
        it_=
          new DimducTextField(
            appli_,
            "de la d�formation" + (ied + 1),
            tf_courbeED_def[ied]);
        it_.positifReel();
        if (it_.positif_) {
          // verification croissance des elements du tableau des deformations 2 � 2 :
          if (ied < (nombre_pointsED_ - 1)) {
            if (courbeED_def_[ied + 1] > 0) {
              it_=
                new DimducTextField(
                  appli_,
                  "de la d�formation " + (ied + 1) + " de la courbe ED",
                  "de la d�formation " + (ied + 2) + " de la courbe ED",
                  tf_courbeED_def[ied],
                  tf_courbeED_def[ied + 1]);
              it_.miniInfMaxiReels();
              if (it_.miniInfMaxi_) {
                courbeED_def_[ied]= it_.doubleTextField1();
              }
            } // fin if (courbeED_def_[ied+1] > 0)
          } // fin if (ied < (nombre_pointsED_ -1))
        } // fin if it_ positif
      } // fin if (source == tf_courbeED_def[ied])
      // efforts : positifs
      if (source == tf_courbeED_eff[ied]) {
        it_=
          new DimducTextField(
            appli_,
            "de l'effort n� " + (ied + 1),
            tf_courbeED_eff[ied]);
        it_.positifReel();
        if (it_.positif_) {
          courbeED_eff_[ied]= it_.doubleTextField1();
        }
      } // fin if (source == tf_courbeED_eff[ied])
    } // fin boucle for
  } // fin void focusLost
  // updatePanels *****************************
  public void updatePanels(final FudaaProjet project_)  // ******************************************
  {
    setTextes(
      (SParametres01)project_.getParam(DimducResource.DIMDUC01),
      (SParametres02)project_.getParam(DimducResource.DIMDUC02));
  }
  // **************************************************************************************************
  // void setTextes : rappel donnees Sparametres  -> ecrans + initialisations variables + controles : *
  // **************************************************************************************************
  private void setTextes(
    final SParametres01 _paramsDimduc01,
    final SParametres02 _paramsDimduc02) {
    // _paramsDimduc01 :
    if (_paramsDimduc01 != null) {
      // onglet 1 : 1/1
      // tf_cote_tete :
      tf_cote_tete.setText("" + _paramsDimduc01.coteTete);
      cote_tete_= Double.valueOf(tf_cote_tete.getText()).doubleValue();
      // tf_fleche_tete :
      tf_fleche_tete.setText("" + _paramsDimduc01.flecheMaxiTete);
      fleche_tete_= Double.valueOf(tf_fleche_tete.getText()).doubleValue();
      // mise en coherence existence fleche et valeur fleche
      if (fleche_tete_ > 0) {
        tf_fleche_tete.setEditable(true);
        jc_fleche_tete.setSelectedItem("Oui");
      }
      // rafraichissement affichage
      tf_fleche_tete.setText("" + _paramsDimduc01.flecheMaxiTete);
      fleche_tete_= Double.valueOf(tf_fleche_tete.getText()).doubleValue();
      // onglet 2 : 1/2
      // diametres
      tf_diametre_mini.setText("" + _paramsDimduc01.diametreMini);
      diametre_mini_= Double.valueOf(tf_diametre_mini.getText()).doubleValue();
      tf_diametre_maxi.setText("" + _paramsDimduc01.diametreMaxi);
      diametre_maxi_= Double.valueOf(tf_diametre_maxi.getText()).doubleValue();
      tf_diametre_pas.setText("" + _paramsDimduc01.diametrePas);
      diametre_pas_= Double.valueOf(tf_diametre_pas.getText()).doubleValue();
      // epaisseurs
      tf_epaisseur_mini.setText("" + _paramsDimduc01.epaisseurMini);
      epaisseur_mini_=
        Double.valueOf(tf_epaisseur_mini.getText()).doubleValue();
      tf_epaisseur_maxi.setText("" + _paramsDimduc01.epaisseurMaxi);
      epaisseur_maxi_=
        Double.valueOf(tf_epaisseur_maxi.getText()).doubleValue();
      tf_epaisseur_pas.setText("" + _paramsDimduc01.epaisseurPas);
      epaisseur_pas_= Double.valueOf(tf_epaisseur_pas.getText()).doubleValue();
      tf_epaisseur_cor.setText("" + _paramsDimduc01.epaisseurCor);
      epaisseur_cor_= Double.valueOf(tf_epaisseur_cor.getText()).doubleValue();
      // contraintes
      jc_nb_sigma.setSelectedIndex(_paramsDimduc01.nbSigma);
      nombre_contraintes_= jc_nb_sigma.getSelectedIndex() + 1;
      afficherContraintes(nombre_contraintes_);
      // onglet 3 : 1/2
      jc_nb_sols.setSelectedIndex(_paramsDimduc01.nbCouches);
      nombre_sols_= jc_nb_sols.getSelectedIndex() + 1;
      afficherSols(nombre_sols_);
      // onglet 4 : 1/2
      jc_nb_accostages.setSelectedIndex(_paramsDimduc01.nbAccostages);
      nombre_accostages_= jc_nb_accostages.getSelectedIndex() + 1;
      afficherAccostages(nombre_accostages_);
    } // fin if  _paramsDimduc01!=null1
    // paramsDimduc02 :
    if (_paramsDimduc02 != null) {
      // onglet 2 : 2/2
      // nombre tubes :
      jc_nb_tubes.setSelectedIndex(_paramsDimduc02.ensembleTubes.nombreTubes);
      nombre_tubes_= jc_nb_tubes.getSelectedIndex() + 1;
      afficherEntreAxe(nombre_tubes_);
      tf_entre_axes.setText("" + _paramsDimduc02.ensembleTubes.entreAxe);
      entre_axes_= Double.valueOf(tf_entre_axes.getText()).doubleValue();
      // module d'Young :
      tf_module_young.setText(
        "" + _paramsDimduc02.ensembleTubes.moduleYoungTubes);
      young_= Double.valueOf(tf_module_young.getText()).doubleValue();
      // recuperation donnees contraintes :
      for (isigma= 0; isigma < 8; isigma++) {
        tf_contrainte[isigma].setText(
          "" + _paramsDimduc02.contraintesAdm[isigma].contrainteAdm);
        contrainte_[isigma]=
          Integer.valueOf(tf_contrainte[isigma].getText()).intValue();
      } // fin boucle for
      // onglet 3 : 2/2
      // chargements champs : maxi neutralises dans cette version
      // sol 1 :
      tf_cote_sup_1.setText("" + _paramsDimduc02.couchesSol[0].coteSuperieure);
      cote_sup_1_= Double.valueOf(tf_cote_sup_1.getText()).doubleValue();
      tf_butee_mini_1.setText("" + _paramsDimduc02.couchesSol[0].buteeMini);
      butee_mini_1_= Double.valueOf(tf_butee_mini_1.getText()).doubleValue();
      tf_cohesion_mini_1.setText(
        "" + _paramsDimduc02.couchesSol[0].cohesionMini);
      cohesion_mini_1_=
        Double.valueOf(tf_cohesion_mini_1.getText()).doubleValue();
      tf_poids_mini_1.setText("" + _paramsDimduc02.couchesSol[0].poidsMini);
      poids_mini_1_= Double.valueOf(tf_poids_mini_1.getText()).doubleValue();
      // sol 2
      tf_cote_sup_2.setText("" + _paramsDimduc02.couchesSol[1].coteSuperieure);
      cote_sup_2_= Double.valueOf(tf_cote_sup_2.getText()).doubleValue();
      tf_butee_mini_2.setText("" + _paramsDimduc02.couchesSol[1].buteeMini);
      butee_mini_2_= Double.valueOf(tf_butee_mini_2.getText()).doubleValue();
      tf_cohesion_mini_2.setText(
        "" + _paramsDimduc02.couchesSol[1].cohesionMini);
      cohesion_mini_2_=
        Double.valueOf(tf_cohesion_mini_2.getText()).doubleValue();
      tf_poids_mini_2.setText("" + _paramsDimduc02.couchesSol[1].poidsMini);
      poids_mini_2_= Double.valueOf(tf_poids_mini_2.getText()).doubleValue();
      // onglet 4 : 2/2
      // affichage valeurs accostages, en boucle for :
      for (iacc= 0; iacc < 10; iacc++) {
        tf_cote_acc[iacc].setText(
          "" + _paramsDimduc02.accostages[iacc].coteAccostage);
        cote_acc_[iacc]=
          Double.valueOf(tf_cote_acc[iacc].getText()).doubleValue();
        tf_energie_acc[iacc].setText(
          "" + _paramsDimduc02.accostages[iacc].energieAccostage);
        energie_acc_[iacc]=
          Double.valueOf(tf_energie_acc[iacc].getText()).doubleValue();
        tf_reaction_acc[iacc].setText(
          "" + _paramsDimduc02.accostages[iacc].reactionAdmissible);
        reaction_acc_[iacc]=
          Double.valueOf(tf_reaction_acc[iacc].getText()).doubleValue();
        if (reaction_acc_[iacc] > 0) {
          jc_reaction_acc_[iacc].setSelectedItem("Oui");
          tf_reaction_acc[iacc].setEditable(true);
          tf_reaction_acc[iacc].setText(
            "" + _paramsDimduc02.accostages[iacc].reactionAdmissible);
        } // fin if (reaction_acc_[iacc] > 0)
        else if (reaction_acc_[iacc] <= 0) {
          jc_reaction_acc_[iacc].setSelectedItem("Non");
          effaceJtfD(tf_reaction_acc[iacc], reaction_acc_[iacc]);
        } // fin else if
      } // fin boucle for
      // onglet 5 : 2/2
      // defense
      jc_nb_pointsED.setSelectedIndex(_paramsDimduc01.nbPointsED);
      nombre_pointsED_= jc_nb_pointsED.getSelectedIndex();
      // mise en coherence nb points, existence defense et affichages :
      if (nombre_pointsED_ > 0) {
        jc_defense.setSelectedItem("Oui");
        obj_newOption= "Oui";
      } else {
        jc_defense.setSelectedItem("Non");
        obj_newOption= "Non";
      }
      afficherDefense(obj_newOption);
      // mises a jour :
      tf_hauteur_defense.setText("" + _paramsDimduc02.defense.hauteurDefense);
      hauteur_defense_=
        Double.valueOf(tf_hauteur_defense.getText()).doubleValue();
      // courbe ED :
      tf_courbeED_cote_maxi.setText(
        "" + _paramsDimduc02.defense.courbesED[0].coteMaxi);
      courbeED_cote_maxi_=
        Double.valueOf(tf_courbeED_cote_maxi.getText()).doubleValue();
      tf_courbeED_cote_mini.setText(
        "" + _paramsDimduc02.defense.courbesED[0].coteMini);
      courbeED_cote_mini_=
        Double.valueOf(tf_courbeED_cote_mini.getText()).doubleValue();
      if (courbeED_cote_mini_ < courbeED_cote_maxi_) {
        jc_iv.setSelectedItem("Oui");
      } else {
        jc_iv.setSelectedItem("Non");
      }
      // rafraichissement ecran :
      tf_courbeED_cote_maxi.setText(
        "" + _paramsDimduc02.defense.courbesED[0].coteMaxi);
      tf_courbeED_cote_mini.setText(
        "" + _paramsDimduc02.defense.courbesED[0].coteMini);
      // System.out.println(" courbeED_cote_mini_ = " + courbeED_cote_mini_ + " , courbeED_cote_maxi_ = " + courbeED_cote_maxi_  );
      // rebelote pour affichage points ED:
      jc_nb_pointsED.setSelectedIndex(_paramsDimduc01.nbPointsED);
      nombre_pointsED_= jc_nb_pointsED.getSelectedIndex();
      afficherCourbeED(nombre_pointsED_);
      // points courbe ED : 1 a 7 : en boucle for
      for (ied= 0; ied < 7; ied++) {
        tf_courbeED_def[ied].setText(
          ""
            + _paramsDimduc02
              .defense
              .courbesED[0]
              .pointsED[ied]
              .deformationDefense);
        courbeED_def_[ied]=
          Double.valueOf(tf_courbeED_def[ied].getText()).doubleValue();
        tf_courbeED_eff[ied].setText(
          ""
            + _paramsDimduc02.defense.courbesED[0].pointsED[ied].effortDefense);
        courbeED_eff_[ied]=
          Double.valueOf(tf_courbeED_eff[ied].getText()).doubleValue();
      } // fin boucle for
    } // fin if _paramsDimduc02!=null
  } // fin void setTextes
  // getTextes01. transfert donnees ecrans saisie  -> SParametres01
  public SParametres01 getTextes01() {
    SParametres01 paramsDimduc01= new SParametres01();
    try {
      // onglet 1 : 1/1
      paramsDimduc01.coteTete=
        Double.valueOf(tf_cote_tete.getText()).doubleValue();
      paramsDimduc01.flecheMaxiTete=
        Double.valueOf(tf_fleche_tete.getText()).doubleValue();
      // onglet 2 : 1/2
      paramsDimduc01.diametreMini=
        Double.valueOf(tf_diametre_mini.getText()).doubleValue();
      paramsDimduc01.diametreMaxi=
        Double.valueOf(tf_diametre_maxi.getText()).doubleValue();
      paramsDimduc01.diametrePas=
        Double.valueOf(tf_diametre_pas.getText()).doubleValue();
      paramsDimduc01.epaisseurMini=
        Double.valueOf(tf_epaisseur_mini.getText()).doubleValue();
      paramsDimduc01.epaisseurMaxi=
        Double.valueOf(tf_epaisseur_maxi.getText()).doubleValue();
      paramsDimduc01.epaisseurPas=
        Double.valueOf(tf_epaisseur_pas.getText()).doubleValue();
      paramsDimduc01.epaisseurCor=
        Double.valueOf(tf_epaisseur_cor.getText()).doubleValue();
      paramsDimduc01.nbSigma= jc_nb_sigma.getSelectedIndex();
      // onglet 3 : 1/2
      paramsDimduc01.nbCouches= jc_nb_sols.getSelectedIndex();
      // onglet 4 : 1/2
      paramsDimduc01.nbAccostages= jc_nb_accostages.getSelectedIndex();
      // onglet 5 : 1/2
      paramsDimduc01.nbPointsED= jc_nb_pointsED.getSelectedIndex();
    } // fin try
    catch (final NumberFormatException e) {
      paramsDimduc01= null;
      System.err.println("ParamsDimduc01 : NumberFormatException");
    }
    return paramsDimduc01;
  }
  // getTextes02 : transfert donnees ecrans -> SParametres02
  public SParametres02 getTextes02() {
    // init :
    SParametres02 paramsDimduc02= new SParametres02();
    paramsDimduc02.ensembleTubes= new SEnsembleTubes();
    paramsDimduc02.ensembleTubes.tubes= new STube[1];
    paramsDimduc02.ensembleTubes.tubes[0]= new STube();
    paramsDimduc02.contraintesAdm= new SContrainteAdmissible[8];
    paramsDimduc02.couchesSol= new SCoucheSol[2];
    paramsDimduc02.accostages= new SAccostage[10];
    paramsDimduc02.defense= new SDefense();
    paramsDimduc02.defense.courbesED= new SCourbeEffortDeformation[7];
    try {
      // onglet 2 : 2/2  : ok
      paramsDimduc02.ensembleTubes.nombreTubes= jc_nb_tubes.getSelectedIndex();
      paramsDimduc02.ensembleTubes.entreAxe=
        Double.valueOf(tf_entre_axes.getText()).doubleValue();
      paramsDimduc02.ensembleTubes.moduleYoungTubes=
        Double.valueOf(tf_module_young.getText()).doubleValue();
      // contraintes en boucle for :
      for (isigma= 0; isigma < 8; isigma++) {
        paramsDimduc02.contraintesAdm[isigma]= new SContrainteAdmissible();
        paramsDimduc02.contraintesAdm[isigma].contrainteAdm=
          Integer.valueOf(tf_contrainte[isigma].getText()).intValue();
      } // fin boucle for
      // onglet 3 : 2/2 : ok
      // sols : 1 ou 2 : maxis neutralises dans cette version
      paramsDimduc02.couchesSol[0]= new SCoucheSol();
      paramsDimduc02.couchesSol[0].coteSuperieure=
        Double.valueOf(tf_cote_sup_1.getText()).doubleValue();
      paramsDimduc02.couchesSol[0].buteeMini=
        Double.valueOf(tf_butee_mini_1.getText()).doubleValue();
      // paramsDimduc02.couchesSol[0].buteeMaxi = Double.valueOf(tf_butee_maxi_1.getText()).doubleValue();
      paramsDimduc02.couchesSol[0].cohesionMini=
        Double.valueOf(tf_cohesion_mini_1.getText()).doubleValue();
      // paramsDimduc02.couchesSol[0].cohesionMaxi = Double.valueOf(tf_cohesion_maxi_1.getText()).doubleValue();
      paramsDimduc02.couchesSol[0].poidsMini=
        Double.valueOf(tf_poids_mini_1.getText()).doubleValue();
      // paramsDimduc02.couchesSol[0].poidsMaxi = Double.valueOf(tf_poids_maxi_1.getText()).doubleValue();
      // sol 2 :
      paramsDimduc02.couchesSol[1]= new SCoucheSol();
      paramsDimduc02.couchesSol[1].coteSuperieure=
        Double.valueOf(tf_cote_sup_2.getText()).doubleValue();
      paramsDimduc02.couchesSol[1].buteeMini=
        Double.valueOf(tf_butee_mini_2.getText()).doubleValue();
      // paramsDimduc02.couchesSol[1].buteeMaxi = Double.valueOf(tf_butee_maxi_2.getText()).doubleValue();
      paramsDimduc02.couchesSol[1].cohesionMini=
        Double.valueOf(tf_cohesion_mini_2.getText()).doubleValue();
      // paramsDimduc02.couchesSol[1].cohesionMaxi = Double.valueOf(tf_cohesion_maxi_2.getText()).doubleValue();
      paramsDimduc02.couchesSol[1].poidsMini=
        Double.valueOf(tf_poids_mini_2.getText()).doubleValue();
      // paramsDimduc02.couchesSol[1].poidsMaxi = Double.valueOf(tf_poids_maxi_2.getText()).doubleValue();
      // */
      // onglet 4 : 2/2  ok
      // accostages en boucle for :
      for (iacc= 0; iacc < 10; iacc++) {
        paramsDimduc02.accostages[iacc]= new SAccostage();
        paramsDimduc02.accostages[iacc].coteAccostage=
          Double.valueOf(tf_cote_acc[iacc].getText()).doubleValue();
        paramsDimduc02.accostages[iacc].energieAccostage=
          Double.valueOf(tf_energie_acc[iacc].getText()).doubleValue();
        paramsDimduc02.accostages[iacc].reactionAdmissible=
          Double.valueOf(tf_reaction_acc[iacc].getText()).doubleValue();
      } // fin boucle for
      // onglet 5 : 2/2
      paramsDimduc02.defense.hauteurDefense=
        Double.valueOf(tf_hauteur_defense.getText()).doubleValue();
      paramsDimduc02.defense.nombreCourbesED= nombre_courbesED_;
      // courbe ED :
      paramsDimduc02.defense.courbesED[0]= new SCourbeEffortDeformation();
      paramsDimduc02.defense.courbesED[0].coteMaxi=
        Double.valueOf(tf_courbeED_cote_maxi.getText()).doubleValue();
      paramsDimduc02.defense.courbesED[0].coteMini=
        Double.valueOf(tf_courbeED_cote_mini.getText()).doubleValue();
      paramsDimduc02.defense.courbesED[0].pointsED=
        new SPointEffortDeformation[7];
      // boucle sur les 7 points :
      for (ied= 0; ied < 7; ied++) {
        paramsDimduc02.defense.courbesED[0].pointsED[ied]=
          new SPointEffortDeformation();
        paramsDimduc02.defense.courbesED[0].pointsED[ied].deformationDefense=
          Double.valueOf(tf_courbeED_def[ied].getText()).doubleValue();
        paramsDimduc02.defense.courbesED[0].pointsED[ied].effortDefense=
          Double.valueOf(tf_courbeED_eff[ied].getText()).doubleValue();
      } // fin boucle for ied
    } // fin try
    catch (final NumberFormatException e) {
      paramsDimduc02= null;
      System.err.println("Parametres 02 : NumberFormatException");
    }
    return paramsDimduc02;
  }
  // constructeur
  public DimducFilleParametres() {
    try {
      jbInit();
    } catch (final Exception e) {
      e.printStackTrace();
    }
  }
  private void jbInit() throws Exception {
    this.setToolTipText("");
    this.setName("");
  } // fin void jbinit
} // fin classe DimducFilleParametres
