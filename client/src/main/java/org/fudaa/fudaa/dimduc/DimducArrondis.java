/*
 * @file         DimducArrondis.java
 * @creation     2001-01-25
 * @modification $Date: 2006-09-19 15:02:07 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.dimduc;
/**
 * @version      $Revision: 1.5 $ $Date: 2006-09-19 15:02:07 $ by $Author: deniger $
 * @author       Christian Barou
 */
public class DimducArrondis {
  // nombre d�cimal � arrondir ; prot�g� :
  protected double a_arrondir_;
  // nombre arrondi ; prot�g� :
  protected double arrondi_;
  // Constructeur :
  public DimducArrondis(final double _a_arrondir) {
    a_arrondir_= _a_arrondir;
  }
  // methode arrondissant un nombre d�cimal � 2 d�cimales :
  public void arrondi2Decimales() {
    arrondi_= a_arrondir_ * 100;
    arrondi_= Math.round(arrondi_);
    arrondi_= (arrondi_ * 0.01);
  }
  // methode retournant la valeur arrondie :
  public double arrondi() {
    return arrondi_;
  }
} // fin class DimducArrondis
