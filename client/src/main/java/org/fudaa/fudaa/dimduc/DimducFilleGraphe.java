/*
 * @file         DimducFilleGraphe.java
 * @creation     2001-01-25
 * @modification $Date: 2006-09-19 15:02:07 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.dimduc;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuInformationsDocument;

import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.ebli.graphe.EbliFilleGraphe;
/**
 * Une fenetre fille pour les graphes Dimduc.$Date: 2001/01/25 : V 1.10
 * sous-classe de BuInternalFrame
 *
 * @version      $Revision: 1.5 $ $Date: 2006-09-19 15:02:07 $ by $Author: deniger $
 * @author       chritian Barou 
 */
public class DimducFilleGraphe
  extends EbliFilleGraphe
  implements InternalFrameListener {
  //private BGraphe graphe;
  // constructeur fenetre pour affichage graphe
  public DimducFilleGraphe(
    final BuCommonImplementation _appli,
    final BuInformationsDocument _id) {
    super("Graphe", true, true, true, true, _appli, _id);
    setBackground(Color.white);
    graphe_= new BGraphe();
    setPreferredSize(new Dimension(640, 480));
    getContentPane().setLayout(new BorderLayout());
    getContentPane().add(graphe_, BorderLayout.CENTER);
    setVisible(true);
    addInternalFrameListener(this);
  }
  public BGraphe getGraphe() {
    return graphe_;
  }
  public void setGraphe(final BGraphe _graphe) {
    getContentPane().removeAll();
    graphe_= _graphe;
    getContentPane().add(graphe_, BorderLayout.CENTER);
    _graphe.repaint();
  }
  /*   public void print(PrintJob _job, Graphics _g)
    {
      BuPrinter.INFO_DOC     =new BuInformationsDocument();
      BuPrinter.INFO_DOC.name=getTitle();
      BuPrinter.INFO_DOC.logo=null;
  
      BuPrinter.printComponent(_job,_g,graphe);
    }
   */
  /**
   *  Non implante
   */
  public void internalFrameClosed(final InternalFrameEvent e) {
    setVisible(false);
    // implementation.getMainPanel().removeInternalFrame(this);
  }
  /**
   *  Non implante
   */
  public void internalFrameActivated(final InternalFrameEvent e) {}
  /**
   *  Non implante
   */
  public void internalFrameDeactivated(final InternalFrameEvent e) {}
  /**
   *  Non implante
   */
  public void internalFrameOpened(final InternalFrameEvent e) {}
  /**
   *  Non implante
   */
  public void internalFrameClosing(final InternalFrameEvent e) {}
  /**
   *  Non implante
   */
  public void internalFrameIconified(final InternalFrameEvent e) {}
  /**
   *  Non implante
   */
  public void internalFrameDeiconified(final InternalFrameEvent e) {}
}
