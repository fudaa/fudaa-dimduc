/*
 * @file         DimducPreferences.java
 * @creation     2001-01-21
 * @modification $Date: 2006-09-19 15:02:07 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.dimduc;
import com.memoire.bu.BuPreferences;
/**
 * Preferences pour Dimduc. sous-classe de BuPreferences
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:02:07 $ by $Author: deniger $
 * @author       Christian Barou 
 */
public class DimducPreferences extends BuPreferences {
  public final static DimducPreferences DIMDUC= new DimducPreferences();
  // verification si objet instance de DimducImplementation
  public void applyOn(final Object _o) {
    if (!(_o instanceof DimducImplementation)) {
      throw new RuntimeException("" + _o + " is not a DimducImplementation.");
    //    DimducImplementation _appli=(DimducImplementation)_o;
    }
  }
}
