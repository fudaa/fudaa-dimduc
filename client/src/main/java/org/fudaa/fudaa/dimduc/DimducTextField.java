/*
 * @file         DimducTextField.java
 * @creation     2001-01-29
 * @modification $Date: 2006-09-19 15:02:07 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.dimduc;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuTextField;
/**
 * controle de saisies de nombres entiers ou reels.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:02:07 $ by $Author: deniger $
 * @author       Christian Barou
 */
public class DimducTextField extends BuCommonImplementation {
  // BuCommonInterface :
  protected BuCommonInterface appli_;
  // DimducImplementation :
  protected DimducImplementation dimduc_;
  // BuInformations
  protected static BuInformationsSoftware isDimduc_=
    new BuInformationsSoftware();
  // Strings nom1_, nom2_ ; protected
  protected String nom1_, nom2_;
  // BuTextField, � convertir puis � contr�ler ; protected :
  protected BuTextField a_controler1_, a_controler2_;
  // booleens positif_, positifOuNul_ ; protecteds :
  protected boolean positif_;
  protected boolean positifOuNul_;
  ;
  protected boolean positif1Des2_;
  protected boolean miniInfMaxi_;
  // entier intTextField_, valeur convertie ; protected :
  protected int intTextField_;
  // reels doubleTextField1_, valeurs converties ; protected :
  protected double doubleTextField1_, doubleTextField2_;
  public BuInformationsSoftware getInformationsSoftware() {
    return isDimduc_;
  }
  // Constructeur 1 champ :
  public DimducTextField(
    final BuCommonInterface _appli,
    final String _nom1,
    final BuTextField _a_controler1) {
    super();
    appli_= _appli;
    dimduc_= (DimducImplementation)appli_.getImplementation();
    nom1_= _nom1;
    a_controler1_= _a_controler1;
  }
  // methode convertissant un champ non vide en entier puis verifiant : positif strict :
  public void positifEntier() {
    try {
      positif_= false;
      intTextField_= 0;
      char car1_= ' ';
      // teste si non vide :
      final int long1_= a_controler1_.getText().length();
      if (long1_ > 0) {
        car1_= a_controler1_.getText().charAt(0);
        if ((car1_ != ' ') && (car1_ != 'e'))          // conversion en entier :
          {
          intTextField_= Integer.parseInt((a_controler1_).getText());
        }
      } // fin if (long1_ > 0 )
      if (intTextField_ <= 0) {
        new BuDialogError(
          dimduc_.getApp(),
          DimducImplementation.DIMDUC_SOFT,
          " la valeur " + nom1_ + " \ndoit �tre strictement positive")
          .activate();
      } // fin  if (intTextField_ <= 0)
      else if (intTextField_ > 0) {
        positif_= true;
      }
    } // fin try
    catch (final Exception e) {
      e.printStackTrace();
      System.err.println("exception dans void positifEntier : " + e);
    }
  } // fin void positifEntier ()
  // methode convertissant un champ non vide en reel puis verifiant : positif strict :
  public void positifReel() {
    try {
      positif_= false;
      doubleTextField1_= 0.0;
      char car1_;
      // teste si non vide :
      final int long1_= a_controler1_.getText().length();
      if (long1_ > 0) {
        car1_= a_controler1_.getText().charAt(0);
        if ((car1_ != ' ') && (car1_ != 'e'))          // conversion en reel :
          {
          doubleTextField1_=
            Double.parseDouble((a_controler1_).getText());
        }
      } // fin if (long1_ > 0 )
      if (doubleTextField1_ <= 0.0) {
        new BuDialogError(
          dimduc_.getApp(),
          DimducImplementation.DIMDUC_SOFT,
          " la valeur " + nom1_ + " \ndoit �tre strictement positive")
          .activate();
      } else if (doubleTextField1_ > 0.0) {
        positif_= true;
      }
    } // fin try
    catch (final Exception e) {
      e.printStackTrace();
      System.err.println("exception dans void positifReel : " + e);
    }
  } // fin void positifReel()
  // methode convertissant un champ non vide en reel puis verifiant : positif ou nul :
  public void positifOuNulReel() {
    try {
      positifOuNul_= false;
      doubleTextField1_= 0.0;
      char car1_;
      // teste si non vide :
      final int long1_= a_controler1_.getText().length();
      if (long1_ > 0) {
        car1_= a_controler1_.getText().charAt(0);
        if ((car1_ != ' ') && (car1_ != 'e'))          // conversion en reel :
          {
          doubleTextField1_=
            Double.parseDouble((a_controler1_).getText());
        }
      } // fin if (long1_ > 0 )
      if (doubleTextField1_ < 0.0) {
        new BuDialogError(
          dimduc_.getApp(),
          DimducImplementation.DIMDUC_SOFT,
          " la valeur " + nom1_ + " \ndoit �tre positive ou nulle")
          .activate();
      } else if (doubleTextField1_ >= 0.0) {
        positifOuNul_= true;
      }
    } // fin try
    catch (final Exception e) {
      e.printStackTrace();
      System.err.println("exception dans void positifOuNulReel : " + e);
    }
  } // fin void positifOuNulReel()
  // Constructeur 2 champs :
  public DimducTextField(
    final BuCommonInterface _appli,
    final String _nom1,
    final String _nom2,
    final BuTextField _a_controler1,
    final BuTextField _a_controler2) {
    super();
    appli_= _appli;
    dimduc_= (DimducImplementation)appli_.getImplementation();
    nom1_= _nom1;
    nom2_= _nom2;
    a_controler1_= _a_controler1;
    a_controler2_= _a_controler2;
  }
  // m�thode verifiant si 1 de 2 reels est positif strict :
  public void positif1Des2Reels() {
    try {
      positif1Des2_= false;
      doubleTextField1_= 0.0;
      doubleTextField2_= 0.0;
      char car1_= ' ';
      char car2_= ' ';
      final int long1_= a_controler1_.getText().length();
      final int long2_= a_controler2_.getText().length();
      // teste si non vide :
      if (long1_ > 0) {
        car1_= a_controler1_.getText().charAt(0);
        if ((car1_ != ' ') && (car1_ != 'e'))          // conversion en reel :
          {
          doubleTextField1_=
            Double.parseDouble((a_controler1_).getText());
          if (long2_ > 0) {
            car2_= a_controler1_.getText().charAt(0);
            if ((car2_ != ' ') && (car1_ != 'e'))              // conversion en reel :
              {
              doubleTextField2_=
                Double.parseDouble((a_controler1_).getText());
            }
          } // fin if (long2_ > 0 )
        } // fin if ((car1_ != ' ') && (car1_ != 'e'))
      } // fin if (long1_ > 0 )
      if ((doubleTextField1_ <= 0.0) && (doubleTextField2_ <= 0.0)) {
        new BuDialogError(
          dimduc_.getApp(),
          DimducImplementation.DIMDUC_SOFT,
          " une des valeurs "
            + nom1_
            + " ou "
            + nom2_
            + " \ndoit �tre positive")
          .activate();
      } else {
        positif1Des2_= true;
      }
    } // fin try
    catch (final Exception e) {
      e.printStackTrace();
      System.err.println("exception dans void positif1Des2Reels : " + e);
    }
  } // fin void positifUnDesDeuxDouble ()
  // m�thode verifiant si le 1er reel est plus petit que le 2eme :
  public void miniInfMaxiReels() {
    try {
      miniInfMaxi_= false;
      doubleTextField1_= 0.0;
      doubleTextField2_= 0.0;
      char car1_= ' ';
      char car2_= ' ';
      final int long1_= a_controler1_.getText().length();
      final int long2_= a_controler2_.getText().length();
      // teste si non vide :
      if (long1_ > 0) {
        car1_= a_controler1_.getText().charAt(0);
        if ((car1_ != ' ') && (car1_ != 'e'))          // conversion en reel :
          {
          doubleTextField1_=
            Double.parseDouble((a_controler1_).getText());
          if (long2_ > 0) {
            car2_= a_controler1_.getText().charAt(0);
            if ((car2_ != ' ') && (car1_ != 'e'))              // conversion en reel :
              {
              doubleTextField2_=
                Double.parseDouble((a_controler2_).getText());
            }
          } // fin if (long2_ > 0 )
        } // fin if ((car1_ != ' ') && (car1_ != 'e'))
      } // fin if (long1_ > 0 )
      if (doubleTextField1_ >= doubleTextField2_) {
        new BuDialogError(
          dimduc_.getApp(),
          DimducImplementation.DIMDUC_SOFT,
          " la valeur "
            + nom1_
            + " doit �tre inf�rieure \n� la valeur "
            + nom2_)
          .activate();
      } // fin if  (doubleTextField1_ >= doubleTextField2_)
 else {
        miniInfMaxi_= true;
      }
    } // fin try
    catch (final Exception e) {
      e.printStackTrace();
      System.err.println("exception dans void miniInfMaxiReels : " + e);
    }
  } // fin void miniInfMaxiReels
  // return
  // m�thode retournant le booleen positif_ :
  public boolean positif() {
    return positif_;
  }
  // m�thode retournant le booleen positifOuNul_ :
  public boolean positifOuNul() {
    return positifOuNul_;
  }
  // m�thode retournant le booleen positif1des2_ :
  public boolean positif1Des2() {
    return positif1Des2_;
  }
  // m�thode retournant le booleen miniInfMaxi_ :
  public boolean miniInfMaxi() {
    return miniInfMaxi_;
  }
  // m�thode retournant l'entier intTextField_ :
  public int intTextField() {
    return intTextField_;
  }
  // m�thode retournant le reel doubleTextField1_ :
  public double doubleTextField1() {
    return doubleTextField1_;
  }
  // m�thode retournant le reel doubleTextField2_ :
  public double doubleTextField2() {
    return doubleTextField2_;
  }
} // fin class DimducTextField
