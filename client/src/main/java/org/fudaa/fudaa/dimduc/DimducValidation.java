/*
 * @file         DimducValidation.java
 * @creation     2001-01-26
 * @modification $Date: 2006-09-19 15:02:07 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.dimduc;
/**
 * @version      $Revision: 1.5 $ $Date: 2006-09-19 15:02:07 $ by $Author: deniger $
 * @author       Christian Barou
 */
public class DimducValidation {
  // DimducFilleParametres ; protegee :
  protected DimducFilleParametres fp_;
  // boolean valide_ ; protegee
  protected boolean valide_= true;
  // chaine des messages d'erreur, protegee :
  protected String mes_erreurs_= " ";
  // Constructeur :
  public DimducValidation(final DimducFilleParametres _fp) {
    fp_= _fp;
  }
  // void validerDonneesProjet :
  public void validerDonneesProjet() {
    // ecran 01 : verif cote_sup_1_ < cote_tete_ :
    if (fp_.cote_sup_1_ >= fp_.cote_tete_) {
      valide_= false;
      mes_erreurs_=
        mes_erreurs_ + "Ecran 1 : Cote sup. sol 1 >= cote t�te ouvrage ! \n";
    }
    // verif fleche_tete_ >= 0 :
    if (fp_.fleche_tete_ < 0) {
      valide_= false;
      mes_erreurs_= mes_erreurs_ + ". Ecran 1 : Fl�che en t�te < 0 ! \n";
    }
    // ecran 02a :
    // (pas diametre et epaisseur > 0) (diametre et epaisseur mini < maxi)
    // (epaisseur corrodee >=0)
    // verif young_ > 0
    if (fp_.young_ <= 0) {
      valide_= false;
      mes_erreurs_= mes_erreurs_ + ". Ecran 2 : module d'Young <= 0 ! \n";
    }
    // verif diametre mini > 0 :
    if (fp_.diametre_mini_ <= 0) {
      valide_= false;
      mes_erreurs_= mes_erreurs_ + ". Ecran 2 : diam�tre mini <= 0 ! \n ";
    }
    // verif diametre mini < diametre maxi :
    if (fp_.diametre_mini_ >= fp_.diametre_maxi_) {
      valide_= false;
      mes_erreurs_=
        mes_erreurs_ + ". Ecran 2 : diam�tre mini >= diam�tre maxi ! \n";
    }
    // verif diametre_pas_ > 0 :
    if (fp_.diametre_pas_ <= 0) {
      valide_= false;
      mes_erreurs_= mes_erreurs_ + ". Ecran 2 : pas des diam�tres <= 0 ! \n";
    }
    // verif epaisseur mini > 0 :
    if (fp_.epaisseur_mini_ <= 0) {
      valide_= false;
      mes_erreurs_= mes_erreurs_ + ". Ecran 2 : �paisseur mini <= 0 ! \n ";
    }
    // verif epaisseur mini < epaisseur maxi :
    if (fp_.epaisseur_mini_ >= fp_.epaisseur_maxi_) {
      valide_= false;
      mes_erreurs_=
        mes_erreurs_ + ". Ecran 2 : �paisseur mini >= �paisseur maxi ! \n ";
    }
    // verif epaisseur_pas_ > 0 :
    if (fp_.epaisseur_pas_ <= 0) {
      valide_= false;
      mes_erreurs_= mes_erreurs_ + ". Ecran 2 : pas des �paisseurs <= 0 ! \n";
    }
    // verif epaisseur_cor_ >= 0 :
    if (fp_.epaisseur_cor_ < 0) {
      valide_= false;
      mes_erreurs_= mes_erreurs_ + ". Ecran 2 : �paisseur corrod�e < 0 ! \n ";
    }
    // ecran 02 b : verif contraintes > 0
    for (byte isigma= 0; isigma < fp_.nombre_contraintes_; isigma++) {
      if (fp_.contrainte_[isigma] <= 0) {
        valide_= false;
        mes_erreurs_=
          mes_erreurs_
            + "Ecran 2 : contrainte n� "
            + (isigma + 1)
            + " <= 0 ! \n";
      }
    } // fin boucle for isigma
    // ecran 03 : 1 ou 2 sols :
    // verif butee_mini_1_ >= 0 :
    if (fp_.butee_mini_1_ < 0) {
      valide_= false;
      mes_erreurs_=
        mes_erreurs_ + ". Ecran sols : Coeff but�e sol n� 1 < 0 ! \n ";
    }
    // verif cohesion_mini_1_ >= 0 :
    if (fp_.cohesion_mini_1_ < 0) {
      valide_= false;
      mes_erreurs_= mes_erreurs_ + ". Ecran sols : coh�sion sol n� 1 < 0 ! \n ";
    }
    // verif coh�sion_mini_1 ou but�e_mini_1 > 0
    if (fp_.butee_mini_1_ == 0) {
      if (fp_.cohesion_mini_1_ == 0) {
        valide_= false;
        mes_erreurs_=
          mes_erreurs_ + ". Ecran sols : but�e et coh�sion sol n� 1 nuls ! \n ";
      }
    }
    // verif poids_mini_1 > 0 :
    if (fp_.poids_mini_1_ <= 0) {
      valide_= false;
      mes_erreurs_= mes_erreurs_ + ". Ecran sol : poids sol n�1 <= 0 ! \n";
    }
    if (fp_.nombre_sols_ == 2) {
      if (fp_.butee_mini_2_ < 0) {
        valide_= false;
        mes_erreurs_=
          mes_erreurs_ + ". Ecran sols : coeff but�e sol n� 2 < 0 ! \n ";
      }
      if (fp_.cohesion_mini_2_ < 0) {
        valide_= false;
        mes_erreurs_=
          mes_erreurs_ + ". Ecran sols : coh�sion sol n� 2 < 0 ! \n ";
      }
      if (fp_.butee_mini_2_ == 0) {
        if (fp_.cohesion_mini_2_ == 0) {
          valide_= false;
          mes_erreurs_=
            mes_erreurs_
              + ". Ecran sols  : but�e et coh�sion sol n� 2 nuls ! \n ";
        }
      }
      if (fp_.poids_mini_2_ <= 0) {
        valide_= false;
        mes_erreurs_= mes_erreurs_ + ". Ecran sols : poids sol 2 <= 0 ! \n";
      }
      // verif cote_sup_1 >= cote_sup_2)
      if (fp_.cote_sup_1_ < fp_.cote_sup_2_) {
        valide_= false;
        mes_erreurs_=
          mes_erreurs_
            + ". Ecran sols : cote sup sol n� 1 < cote sup sol n� 2 ! \n";
      }
    } //  fin if (nombre_sols_ ==2)
    // ecran 04 : accostages : verif cote_sup_1 < cote_acc < cote_tete_)
    // energie_acc > 0, reaction_acc_ > 0
    for (byte iacc= 0; iacc < fp_.nombre_accostages_; iacc++)      // verif cote_acc <= cote_tete_ :
      {
      if (fp_.cote_acc_[iacc] > fp_.cote_tete_) {
        valide_= false;
        mes_erreurs_=
          mes_erreurs_
            + ". Ecran accost. : cote accostage n� "
            + (iacc + 1)
            + " > cote tete ouvrage ! \n";
      }
      // verif cote_acc >= cote_sup_1_ :
      if (fp_.cote_acc_[iacc] < fp_.cote_sup_1_) {
        valide_= false;
        mes_erreurs_=
          mes_erreurs_
            + ". Ecran accost. : cote accostage n� "
            + (iacc + 1)
            + " < cote sup sol 1 ! \n ";
      }
      // verif energie_acc > 0
      if (fp_.energie_acc_[iacc] <= 0) {
        valide_= false;
        mes_erreurs_=
          mes_erreurs_
            + ". Ecran accost. : �nergie accostage n� "
            + (iacc + 1)
            + " <= 0 ! \n";
      }
      // verif reaction_acc_ > 0 :
      if (fp_.jc_reaction_acc_[iacc].getSelectedItem().equals("Oui")) {
        if (fp_.reaction_acc_[iacc] <= 0) {
          valide_= false;
          mes_erreurs_=
            mes_erreurs_
              + ". Ecran accost. : r�action accostage n� "
              + (iacc + 1)
              + " <= 0 ! \n";
        }
      } // fin if (jc_reaction_acc_[iacc].getSelectedItem().equals("Oui"))
    } // fin boucle for iacc
    // ecran 05 : defense : Si existence : (hauteur >0) (cote mini < cote maxi < cote tete)
    //
    if (fp_.nombre_courbesED_ > 0) // defense existe
      // verif hauteur_defense_ > 0 :
      {
      if (fp_.hauteur_defense_ <= 0) {
        valide_= false;
        mes_erreurs_=
          mes_erreurs_ + "Ecran d�fense : hauteur D�fense <= 0 ! \n";
      } // fin if (hauteur_defense_<= 0)
      // verif cote mini < cote maxi
      if (fp_.jc_iv.getSelectedItem().equals("Oui")) {
        if (fp_.courbeED_cote_mini_ >= fp_.courbeED_cote_maxi_) {
          valide_= false;
          mes_erreurs_=
            mes_erreurs_
              + ". Ecran d�fense : validit� courbe  ED : cote mini >= cote maxi ! \n";
        }
        // verif cote
        if (fp_.courbeED_cote_maxi_ > fp_.cote_tete_) {
          valide_= false;
          mes_erreurs_=
            mes_erreurs_
              + ". Ecran d�fense : validit� courbe  ED : Cote maxi > cote tete ouvrage ! \n";
        }
      } // fin  if (jc_iv.getSelectedItem().equals("Oui"))
      // (deformation >= 0 et croissantes), (efforts > 0)
      // 1ere def trait�e hors boucle :
      if (fp_.courbeED_def_[0] <= 0.0) {
        valide_= false;
        mes_erreurs_=
          mes_erreurs_
            + ". Ecran d�fense : d�formation n� 1 = "
            + fp_.courbeED_def_[0]
            + " <= 0 ! \n";
      }
      for (byte ied= 0; ied < (fp_.nombre_pointsED_ - 1); ied++) {
        if (fp_.courbeED_def_[ied] > fp_.courbeED_def_[ied + 1]) {
          valide_= false;
          mes_erreurs_=
            mes_erreurs_
              + ". Ecran d�fense : courbe ED : d�formation n� "
              + (ied + 1)
              + " = "
              + fp_.courbeED_def_[ied]
              + " > D�formation n� "
              + (ied + 2)
              + " = "
              + fp_.courbeED_def_[ied
              + 1]
              + " \n";
        } // fin if
        if (fp_.courbeED_eff_[ied] <= 0.0) {
          valide_= false;
          mes_erreurs_=
            mes_erreurs_
              + ". Ecran d�fense :  courbe ED : effort n� "
              + (ied + 1)
              + " = "
              + fp_.courbeED_eff_[ied]
              + " <= 0 ! \n";
        } // fin if (courbeED_eff_[ied] < 0.0)
      } // fin boucle for ied
      // dernier effort trait� hors boucle for
      if (fp_.courbeED_eff_[fp_.nombre_pointsED_ - 1] <= 0.0) {
        valide_= false;
        mes_erreurs_=
          mes_erreurs_
            + ". Ecran d�fense : courbe ED : effort n� "
            + (fp_.nombre_pointsED_)
            + " = "
            + fp_.courbeED_eff_[fp_.nombre_pointsED_
            - 1]
            + " <= 0 ! ";
      } // fin if (courbeED_eff_[nombre_pointsED_] <0)
    } // fin if (nombre_courbesED_ > 0)
  } // fin void validerDonneesProjet ()
  // methode retournant le bool�en valide_ :
  public boolean valide() {
    return valide_;
  }
  // methode retournant les messages d'erreurs :
  public String mesErreurs() {
    return mes_erreurs_;
  }
} // fin classe
