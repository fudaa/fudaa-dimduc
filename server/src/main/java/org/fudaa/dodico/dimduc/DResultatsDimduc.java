/*
 * @creation     1999-06-01
 * @modification $Date: 2007-03-27 16:09:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.dimduc;

import org.fudaa.ctulu.CtuluLibArray;

import org.fudaa.dodico.corba.dimduc.IResultatsDimduc;
import org.fudaa.dodico.corba.dimduc.IResultatsDimducOperations;
import org.fudaa.dodico.corba.dimduc.SSollicitationAccostage;
import org.fudaa.dodico.corba.dimduc.STableauMoments;
import org.fudaa.dodico.corba.ducalbe.SPointEffortDeformation;

import org.fudaa.dodico.calcul.DResultats;
import org.fudaa.dodico.objet.CDodico;

/**
 * Classe d'implataion de l'interface IResultatsDimduc gerant les resultats du
 * code de calcul <code>dimduc</code>.
 *
 * @version $Id: DResultatsDimduc.java,v 1.12 2007-03-27 16:09:59 deniger Exp $
 * @author Christian Barou
 */
public class DResultatsDimduc extends DResultats implements IResultatsDimduc, IResultatsDimducOperations {
  // private String path;
  /**
   * Structures concernant les points de la courbe effort/deformation.
   */
  private SPointEffortDeformation[] pointsEDL_;

  /**
   * Structures concernant les points de la courbe effort/deformation.
   */
  private SPointEffortDeformation[] pointsEDC_;

  /**
   * Structures decrivant les moments legers.
   */
  private STableauMoments[] tableauMomentsLeger_;

  /**
   * Structures decrivant les moments court.
   */
  private STableauMoments[] tableauMomentsCourt_;

  /**
   * Structure decrivant les sollicitations d'accostage.
   */
  private SSollicitationAccostage[] sollicitationsAccostage_;

  /**
   * Constructeur vide.
   */
  public DResultatsDimduc() {
    super();
  }

  private String erreur_;

  public String erreur() {
    return erreur_;
  }

  public void erreur(final String _newErreur) {
    erreur_ = _newErreur;
  }

  public boolean estVide() {
    return CtuluLibArray.isEmpty(pointsEDC_) && CtuluLibArray.isEmpty(pointsEDL_)
        && CtuluLibArray.isEmpty(tableauMomentsCourt_) && CtuluLibArray.isEmpty(tableauMomentsLeger_);
  }

  /**
   * On remet a null tous les résultats.
   */
  public void initialiseResultats() {
    pointsEDC_ = null;
    pointsEDL_ = null;
    tableauMomentsCourt_ = null;
    tableauMomentsLeger_ = null;
    sollicitationsAccostage_ = null;
  }

  /**
   * Initialise le tableau <code>pointsEDL</code> de la courbe
   * effort/deformation.
   */
  private void litPointsEDL() {
    try {
      pointsEDL_ = new SPointEffortDeformation[] {};
    } catch (final Exception e) {
      System.err.println(e);
      CDodico.exception(this, e);
    }
  }

  /**
   * Appelle <code>litPointsEDL()</code>.
   */
  private void analysePointsEDL() {
    litPointsEDL();
  }

  /**
   * Initialise le tableau <code>pointsEDC</code> des efforts de deformation.
   */
  private void litPointsEDC() {
    try {
      pointsEDC_ = new SPointEffortDeformation[] {};
    } catch (final Exception e) {
      System.err.println(e);
      CDodico.exception(this, e);
    }
  }

  /**
   * Appelle <code>litPointsEDC</code>.
   */
  private void analysePointsEDC() {
    litPointsEDC();
  }

  /**
   * appelle la methode <code>litTableauMomentsLeger()</code>.
   *
   * @see #litTableauMomentsLeger()
   */
  private void analyseTableauMomentsLeger() {
    litTableauMomentsLeger();
  }

  /**
   * Initialise le tableau des moments legers.
   */
  private void litTableauMomentsLeger() {
    try {
      tableauMomentsLeger_ = new STableauMoments[] {};
    } catch (final Exception e) {
      System.err.println(e);
      CDodico.exception(this, e);
    }
  }

  /**
   * Appelle <code>litTableauMomentsCourt()</code>.
   *
   * @see #litTableauMomentsCourt()
   */
  private void analyseTableauMomentsCourt() {
    litTableauMomentsCourt();
  }

  /**
   * Initialise le tableau des moments courts.
   */
  private void litTableauMomentsCourt() {
    try {
      tableauMomentsCourt_ = new STableauMoments[] {};
    } catch (final Exception e) {
      System.err.println(e);
      CDodico.exception(this, e);
    }
  }

  /**
   * Initialise le tableau des sollicitation d'accostage.
   */
  private void litSollicitationAccostage() {
    try {
      sollicitationsAccostage_ = new SSollicitationAccostage[] {};
    } catch (final Exception e) {
      System.err.println(e);
      CDodico.exception(this, e);
    }
  }

  /**
   * Appelle <code>litSollicitationAccostage()</code>.
   *
   * @see #litSollicitationAccostage()
   */
  private void analyseSollicitationAccostage() {
    litSollicitationAccostage();
  }

  /**
   * Modifie les point effort/deformation <code>_pointsEDL</code>.
   *
   */
  public void pointsEDL(final SPointEffortDeformation[] _pointsEDL) {
    pointsEDL_ = _pointsEDL;
  }

  /**
   * Renvoie la structure decrivant la les points de la courbe "EDL"
   * effort/deformation.
   *
   * @return structure interne apres analyse par <code>analysePointsEDL</code>.
   * @see #analysePointsEDL()
   */
  public SPointEffortDeformation[] pointsEDL() {
    if (pointsEDL_ == null) {
      analysePointsEDL();
    }
    return pointsEDL_;
  }

  /**
   * Modifie les points "EDC" de la structure Effort/deformation.
   *
   * @param _pointsEDC
   *          nouvelle structure.
   */
  public void pointsEDC(final SPointEffortDeformation[] _pointsEDC) {
    pointsEDC_ = _pointsEDC;
  }

  /**
   * Renvoie la structure des points de la courbe effort/deformation.
   */
  public SPointEffortDeformation[] pointsEDC() {
    if (pointsEDC_ == null) {
      analysePointsEDC();
    }
    return pointsEDC_;
  }

  /**
   * Modifie le tableau des moments leger.
   *
   * @param _tablMomentsLeger
   *          le nouveau tableau.
   */
  public void tableauMomentsLeger(final STableauMoments[] _tablMomentsLeger) {
    tableauMomentsLeger_ = _tablMomentsLeger;
  }

  public STableauMoments[] tableauMomentsLeger() {
    if (tableauMomentsLeger_ == null) {
      analyseTableauMomentsLeger();
    }
    return tableauMomentsLeger_;
  }

  /**
   * Tableau moments duc + court.
   *
   * @param _tablMomentsCourt
   */
  public void tableauMomentsCourt(final STableauMoments[] _tablMomentsCourt) {
    tableauMomentsCourt_ = _tablMomentsCourt;
  }

  /**
   * Renvoie le tableau des moments courts apres
   * <code>analyseTableauMomentsCourt()</code>.
   *
   * @see #analyseTableauMomentsCourt()
   */
  public STableauMoments[] tableauMomentsCourt() {
    if (tableauMomentsCourt_ == null) {
      analyseTableauMomentsCourt();
    }
    return tableauMomentsCourt_;
  }

  /**
   * renvoie le tableau des sollicitations d'accostage apres analyse:
   * <code>analyseSollicitationAccostage()</code>.
   *
   * @see #analyseSollicitationAccostage()
   */
  public SSollicitationAccostage[] sollicitationsAccostage() {
    if (sollicitationsAccostage_ == null) {
      analyseSollicitationAccostage();
    }
    return sollicitationsAccostage_;
  }

  /**
   * Modifie la structure des sollicitations d'acostage.
   *
   * @param _sollicit
   *          la nouvelle structure.
   */
  public void sollicitationsAccostage(final SSollicitationAccostage[] _sollicit) {
    sollicitationsAccostage_ = _sollicit;
  }
}
